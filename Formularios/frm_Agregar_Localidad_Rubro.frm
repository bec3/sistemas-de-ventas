VERSION 5.00
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "mshflxgd.ocx"
Begin VB.Form frm_Agregar_Localidad_Rubro 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   6345
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   3255
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "frm_Agregar_Localidad_Rubro.frx":0000
   ScaleHeight     =   6345
   ScaleWidth      =   3255
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame1 
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   615
      Left            =   120
      TabIndex        =   5
      Top             =   120
      Width           =   3015
      Begin VB.TextBox txtnuevo 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   6
         Top             =   120
         Width           =   2775
      End
      Begin VB.Line Line1 
         X1              =   0
         X2              =   3000
         Y1              =   0
         Y2              =   0
      End
      Begin VB.Line Line4 
         X1              =   0
         X2              =   3000
         Y1              =   600
         Y2              =   600
      End
      Begin VB.Line Line10 
         X1              =   0
         X2              =   0
         Y1              =   0
         Y2              =   600
      End
      Begin VB.Line Line11 
         X1              =   3000
         X2              =   3000
         Y1              =   0
         Y2              =   600
      End
   End
   Begin VB.Frame Frame3 
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      Caption         =   "Frame3"
      Height          =   615
      Left            =   120
      TabIndex        =   2
      Top             =   840
      Width           =   3015
      Begin VB.CommandButton cmdsalir 
         Caption         =   "Salir"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1560
         TabIndex        =   4
         Top             =   120
         Width           =   1335
      End
      Begin VB.CommandButton cmdagregar 
         Caption         =   "Agregar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   3
         Top             =   120
         Width           =   1335
      End
      Begin VB.Shape Shape1 
         Height          =   615
         Left            =   0
         Top             =   0
         Width           =   3015
      End
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   4695
      Left            =   120
      TabIndex        =   0
      Top             =   1560
      Width           =   3015
      Begin MSHierarchicalFlexGridLib.MSHFlexGrid FGDatos 
         Height          =   4455
         Left            =   120
         TabIndex        =   1
         Top             =   120
         Width           =   2775
         _ExtentX        =   4895
         _ExtentY        =   7858
         _Version        =   393216
         FixedCols       =   0
         ScrollBars      =   2
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _NumberOfBands  =   1
         _Band(0).Cols   =   2
      End
      Begin VB.Line Line8 
         X1              =   3000
         X2              =   3000
         Y1              =   0
         Y2              =   4680
      End
      Begin VB.Line Line7 
         X1              =   0
         X2              =   0
         Y1              =   0
         Y2              =   4680
      End
      Begin VB.Line Line6 
         X1              =   0
         X2              =   3000
         Y1              =   4680
         Y2              =   4680
      End
      Begin VB.Line Line2 
         X1              =   0
         X2              =   3000
         Y1              =   0
         Y2              =   0
      End
   End
End
Attribute VB_Name = "frm_Agregar_Localidad_Rubro"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Sub Localidad()
FGDatos.Cols = 1
FGDatos.Rows = 1
FGDatos.ColWidth(0) = 2770
FGDatos.TextArray(0) = "Localidad"
cn.Open
Set rs = cn.Execute("Listado_Localidad")
Do While rs.EOF = False
FGDatos.Rows = FGDatos.Rows + 1
FGDatos.TextMatrix(FGDatos.Rows - 1, 0) = rs!Localidad
rs.MoveNext
Loop
rs.Close
Set rs = Nothing
cn.Close
FGDatos.Col = 0
FGDatos.CellBackColor = &H8000000F
End Sub
Sub Rubro()
FGDatos.Cols = 1
FGDatos.Rows = 1
FGDatos.ColWidth(0) = 2770
FGDatos.TextArray(0) = "Rubro"
cn.Open
Set rs = cn.Execute("Listado_Rubro")
Do While rs.EOF = False
FGDatos.Rows = FGDatos.Rows + 1
FGDatos.TextMatrix(FGDatos.Rows - 1, 0) = rs!Rubro
rs.MoveNext
Loop
rs.Close
Set rs = Nothing
cn.Close
FGDatos.Col = 0
FGDatos.CellBackColor = &H8000000F
End Sub
Private Sub cmdagregar_Click()
If txtnuevo.Text = "" Then
MsgBox ("Campo vacio")
Else
existe = 0
For i = 1 To FGDatos.Rows - 1
If LCase$(txtnuevo.Text) = LCase$(FGDatos.TextMatrix(i, 0)) Then
    MsgBox ("Ya existe")
    existe = 1
End If
Next i
If existe = 0 Then
cn.Open
    If FGDatos.TextMatrix(0, 0) = "Rubro" Then
        Set rs = cn.Execute("insert into rubro(rubro) values('" & txtnuevo.Text & "')")
        Set rs = Nothing
        cn.Close
        Rubro
        mnabmarticulos.Rubro
        a = MsgBox("Desea utilizar este rubro?", vbYesNo + vbInformation)
        If a = vbYes Then
        mnabmarticulos.cmbrubro.Text = txtnuevo.Text
        Unload Me
        End If
    Else
        Set rs = cn.Execute("insert into localidad(localidad) values('" & txtnuevo.Text & "')")
        Set rs = Nothing
        cn.Close
        Localidad
        mnabmclientes.Localidad
        a = MsgBox("Desea utilizar esta localidad?", vbYesNo + vbInformation)
        If a = vbYes Then
        mnabmclientes.cmblocalidad.Text = txtnuevo.Text
        Unload Me
        End If
    End If
End If
End If
End Sub
Private Sub cmdsalir_Click()
a = MsgBox("Esta Seguro?", vbYesNo)
If a = vbYes Then
Unload Me
End If
End Sub

