VERSION 5.00
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "mshflxgd.ocx"
Begin VB.Form frmmenuprincipal 
   BackColor       =   &H00C0C0C0&
   Caption         =   "Menu Principal"
   ClientHeight    =   7740
   ClientLeft      =   300
   ClientTop       =   750
   ClientWidth     =   17055
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   Picture         =   "frmmenuprincipal.frx":0000
   ScaleHeight     =   96.801
   ScaleMode       =   0  'User
   ScaleWidth      =   332.283
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.Timer Timer1 
      Interval        =   1000
      Left            =   22080
      Top             =   240
   End
   Begin VB.Frame Frame4 
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      Caption         =   "Frame4"
      Height          =   2895
      Left            =   120
      TabIndex        =   2
      Top             =   2640
      Width           =   10695
      Begin MSHierarchicalFlexGridLib.MSHFlexGrid FGResumen 
         Height          =   2570
         Left            =   120
         TabIndex        =   3
         Top             =   120
         Width           =   10455
         _ExtentX        =   18441
         _ExtentY        =   4524
         _Version        =   393216
         BackColor       =   16777215
         FixedCols       =   0
         ScrollBars      =   0
         Appearance      =   0
         _NumberOfBands  =   1
         _Band(0).Cols   =   2
      End
      Begin VB.Shape Shape1 
         Height          =   2895
         Left            =   0
         Top             =   0
         Width           =   10695
      End
   End
   Begin VB.Label lblhora 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "hora"
      BeginProperty Font 
         Name            =   "Calibri"
         Size            =   26.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   630
      Left            =   20640
      TabIndex        =   6
      Top             =   120
      Width           =   975
   End
   Begin VB.Label lblfecha 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "fecha"
      BeginProperty Font 
         Name            =   "Calibri"
         Size            =   26.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   630
      Left            =   17760
      TabIndex        =   5
      Top             =   120
      Width           =   1170
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Ultimos movimientos realizados."
      Height          =   240
      Left            =   120
      TabIndex        =   4
      Top             =   2280
      Width           =   2805
   End
   Begin VB.Label lblusuario 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Usuario"
      BeginProperty Font 
         Name            =   "Calibri"
         Size            =   26.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   630
      Left            =   2040
      TabIndex        =   1
      Top             =   120
      Width           =   1635
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Usuario:"
      BeginProperty Font 
         Name            =   "Calibri"
         Size            =   26.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   630
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   1770
   End
   Begin VB.Image Image1 
      Height          =   7440
      Left            =   16080
      Picture         =   "frmmenuprincipal.frx":6FD2
      Top             =   2280
      Width           =   7440
   End
   Begin VB.Menu mnarchivo 
      Caption         =   "Archivo"
      Begin VB.Menu smnsalir 
         Caption         =   "Salir"
         Shortcut        =   ^S
      End
   End
   Begin VB.Menu mnventas 
      Caption         =   "Ventas"
      Begin VB.Menu mnclientes 
         Caption         =   "Clientes"
      End
      Begin VB.Menu mnarticulos 
         Caption         =   "Articulos"
      End
      Begin VB.Menu mncuentacorrienteventa 
         Caption         =   "Cuenta Corriente"
      End
      Begin VB.Menu mnfacturacion 
         Caption         =   "Facturaci�n"
      End
   End
   Begin VB.Menu mncompras 
      Caption         =   "Compras"
      Begin VB.Menu mnproveedores 
         Caption         =   "Proveedores"
      End
      Begin VB.Menu mncuentacorrientecompra 
         Caption         =   "Cuenta Corriente"
      End
      Begin VB.Menu mnregistrodecompras 
         Caption         =   "Registro de Compras"
      End
   End
   Begin VB.Menu mncaja 
      Caption         =   "Caja"
      Begin VB.Menu mnrecibo 
         Caption         =   "Recibo"
      End
      Begin VB.Menu mncontroldecaja 
         Caption         =   "Control de caja"
      End
   End
   Begin VB.Menu mncontabilidad 
      Caption         =   "Contabilidad"
      Begin VB.Menu mnlibrodeiva 
         Caption         =   "Libro de Iva"
         Begin VB.Menu mncomprasiva 
            Caption         =   "Compras"
         End
         Begin VB.Menu mnventasiva 
            Caption         =   "Ventas"
         End
      End
   End
   Begin VB.Menu mnparametros 
      Caption         =   "Parametros"
   End
End
Attribute VB_Name = "frmmenuprincipal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub FGResumen_dblClick()
If FGResumen.Row <> 0 Then
frm_detalle_ventas.Detalle_venta_resumen
frm_detalle_ventas.Show
End If
End Sub

Private Sub mnarticulos_Click()
mnabmarticulos.Show
End Sub
Private Sub mnclientes_Click()
mnabmclientes.Show
End Sub
Private Sub mncuentacorrienteventa_Click()
frm_cuenta_corriente.Show
End Sub
Private Sub mnfacturacion_Click()
frm_facturacion.Show
End Sub
Private Sub mnrecibo_Click()
frm_recibo.Show
End Sub
Private Sub mnventasiva_Click()
frm_libro_iva.Show
End Sub
Private Sub smnsalir_Click()
If MsgBox("Desea Salir del Sistema ", vbInformation + vbYesNo, "Aviso") = vbYes Then
    Unload Me
frminiciosesion.Show
End If
End Sub
Sub Actualizar_Lista()
cn.Open
Set rs = cn.Execute("SELECT TOP 10 CONVERT(varchar, Ventas.fecha, 103) AS Fecha, Ventas.letra_c, tipo_venta.tipo, Ventas.numero, Cliente.nombre,Ventas.neto, Ventas.iva, Ventas.total FROM Ventas INNER JOIN Cliente ON Ventas.idcliente = Cliente.idcliente INNER JOIN tipo_venta ON Ventas.idtipo_venta = tipo_venta.idtipo_venta ORDER BY ventas.idventas DESC")
Set FGResumen.DataSource = rs
cn.Close
Tama�o_Grilla
End Sub
Sub Tama�o_Grilla()
With FGResumen
.Cols = 8
.ColWidth(0) = 1170
.ColWidth(1) = 700
.ColWidth(2) = 1500
.ColWidth(3) = 1500
.ColWidth(4) = 1500
.ColWidth(5) = 1300
.ColWidth(6) = 1300
.ColWidth(7) = 1450
.TextArray(0) = "Fecha"
.TextArray(1) = "Letra"
.TextArray(2) = "Tipo"
.TextArray(3) = "N�mero"
.TextArray(4) = "Cliente"
.TextArray(5) = "Neto"
.TextArray(6) = "Iva"
.TextArray(7) = "Total"
.ColAlignment(5) = flexAlignRightCenter
.ColAlignment(6) = flexAlignRightCenter
.ColAlignment(7) = flexAlignRightCenter
End With
End Sub
Private Sub Form_Load()
Actualizar_Lista
Tama�o_Grilla
lblfecha.Caption = Date
lblhora.Caption = Time
End Sub
Private Sub Timer1_Timer()
lblhora.Caption = Time
End Sub
