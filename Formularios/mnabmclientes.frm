VERSION 5.00
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "mshflxgd.ocx"
Begin VB.Form mnabmclientes 
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Clientes"
   ClientHeight    =   10110
   ClientLeft      =   1140
   ClientTop       =   2205
   ClientWidth     =   12030
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "mnabmclientes.frx":0000
   ScaleHeight     =   10110
   ScaleWidth      =   12030
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame4 
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      Caption         =   "Frame4"
      Height          =   6615
      Left            =   120
      TabIndex        =   37
      Top             =   3360
      Width           =   11775
      Begin MSHierarchicalFlexGridLib.MSHFlexGrid FGClientes 
         Height          =   6375
         Left            =   120
         TabIndex        =   38
         Top             =   120
         Width           =   11535
         _ExtentX        =   20346
         _ExtentY        =   11245
         _Version        =   393216
         FixedCols       =   0
         ScrollBars      =   2
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _NumberOfBands  =   1
         _Band(0).Cols   =   2
      End
      Begin VB.Line Line18 
         X1              =   11760
         X2              =   11760
         Y1              =   6600
         Y2              =   0
      End
      Begin VB.Line Line17 
         BorderColor     =   &H80000007&
         X1              =   0
         X2              =   0
         Y1              =   0
         Y2              =   6600
      End
      Begin VB.Line Line16 
         BorderColor     =   &H80000007&
         X1              =   0
         X2              =   11760
         Y1              =   6600
         Y2              =   6600
      End
      Begin VB.Line Line15 
         BorderColor     =   &H80000007&
         X1              =   0
         X2              =   11760
         Y1              =   0
         Y2              =   0
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      Caption         =   "Datos personales"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000A&
      Height          =   1455
      Left            =   120
      TabIndex        =   9
      Top             =   120
      Width           =   11775
      Begin VB.CommandButton cmdagregarlocalidad 
         Caption         =   "+"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   10680
         TabIndex        =   39
         Top             =   240
         Width           =   375
      End
      Begin VB.TextBox txtcodigo 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1080
         TabIndex        =   0
         Top             =   240
         Width           =   1575
      End
      Begin VB.TextBox txttelefono 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3960
         TabIndex        =   3
         Top             =   600
         Width           =   1935
      End
      Begin VB.TextBox txtdomicilio 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1080
         TabIndex        =   2
         Top             =   600
         Width           =   1575
      End
      Begin VB.TextBox txtnombre 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3960
         TabIndex        =   1
         Top             =   240
         Width           =   1935
      End
      Begin VB.TextBox txtcuil 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1080
         TabIndex        =   4
         Text            =   " "
         Top             =   960
         Width           =   1575
      End
      Begin VB.ComboBox cmbiva 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   7800
         TabIndex        =   7
         Text            =   " [Seleccione una categor�a]"
         Top             =   960
         Width           =   2775
      End
      Begin VB.TextBox txtemail 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3720
         TabIndex        =   5
         Text            =   " "
         Top             =   960
         Width           =   2175
      End
      Begin VB.ComboBox cmblocalidad 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         ItemData        =   "mnabmclientes.frx":6FD2
         Left            =   7800
         List            =   "mnabmclientes.frx":6FD4
         TabIndex        =   6
         Text            =   "[Seleccione una localidad]"
         Top             =   240
         Width           =   2775
      End
      Begin VB.Line Line6 
         X1              =   0
         X2              =   11760
         Y1              =   0
         Y2              =   0
      End
      Begin VB.Line Line5 
         X1              =   11760
         X2              =   11760
         Y1              =   0
         Y2              =   1440
      End
      Begin VB.Line Line4 
         X1              =   0
         X2              =   11880
         Y1              =   1440
         Y2              =   1440
      End
      Begin VB.Line Line3 
         X1              =   0
         X2              =   0
         Y1              =   0
         Y2              =   1440
      End
      Begin VB.Label lblvcategoria 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "*"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   21.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   495
         Left            =   10680
         TabIndex        =   22
         Top             =   960
         Visible         =   0   'False
         Width           =   165
      End
      Begin VB.Label lblvlocalidad 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "*"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   21.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   495
         Left            =   11280
         TabIndex        =   21
         Top             =   240
         Visible         =   0   'False
         Width           =   165
      End
      Begin VB.Label lblvtelefono 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "*"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   21.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   495
         Left            =   6000
         TabIndex        =   20
         Top             =   600
         Visible         =   0   'False
         Width           =   165
      End
      Begin VB.Label lblvnombre 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "*"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   21.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   495
         Left            =   6000
         TabIndex        =   19
         Top             =   240
         Visible         =   0   'False
         Width           =   165
      End
      Begin VB.Label lblvdomicilio 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "*"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   21.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   375
         Left            =   2760
         TabIndex        =   18
         Top             =   600
         Visible         =   0   'False
         Width           =   165
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Codigo"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   0
         Left            =   120
         TabIndex        =   17
         Top             =   240
         Width           =   600
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Condici�n IVA"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   6480
         TabIndex        =   16
         Top             =   960
         Width           =   1230
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Domicilio"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   120
         TabIndex        =   15
         Top             =   600
         Width           =   795
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Cuit"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   120
         TabIndex        =   14
         Top             =   960
         Width           =   345
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Nombre"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   3120
         TabIndex        =   13
         Top             =   240
         Width           =   675
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Telefono"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   3120
         TabIndex        =   12
         Top             =   600
         Width           =   720
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Email"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   3120
         TabIndex        =   11
         Top             =   960
         Width           =   495
      End
      Begin VB.Label Label8 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Localidad"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   6480
         TabIndex        =   10
         Top             =   360
         Width           =   825
      End
   End
   Begin VB.Frame Frame3 
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      Caption         =   "Control"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   120
      TabIndex        =   29
      Top             =   1680
      Width           =   11775
      Begin VB.CommandButton cmdnuevo 
         Caption         =   "Nuevo"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   35
         Top             =   120
         Width           =   1575
      End
      Begin VB.CommandButton cmdmodificar 
         Caption         =   "Modificar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1800
         TabIndex        =   34
         Top             =   120
         Width           =   1575
      End
      Begin VB.CommandButton cmdguardar 
         Caption         =   "Guardar"
         CausesValidation=   0   'False
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   5160
         TabIndex        =   33
         Top             =   120
         Width           =   1575
      End
      Begin VB.CommandButton cmdcancelar 
         Caption         =   "Cancelar"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   6840
         TabIndex        =   32
         Top             =   120
         Width           =   1575
      End
      Begin VB.CommandButton cmdsalir 
         Caption         =   "Salir"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   10080
         TabIndex        =   31
         Top             =   120
         Width           =   1575
      End
      Begin VB.CommandButton cmdeliminar 
         Caption         =   "Eliminar"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3480
         TabIndex        =   30
         Top             =   120
         Width           =   1575
      End
      Begin VB.Line Line10 
         BorderColor     =   &H80000007&
         X1              =   11760
         X2              =   11760
         Y1              =   0
         Y2              =   600
      End
      Begin VB.Line Line9 
         BorderColor     =   &H80000007&
         X1              =   0
         X2              =   0
         Y1              =   0
         Y2              =   600
      End
      Begin VB.Line Line8 
         BorderColor     =   &H80000007&
         X1              =   0
         X2              =   11880
         Y1              =   600
         Y2              =   600
      End
      Begin VB.Line Line7 
         BorderColor     =   &H80000007&
         X1              =   0
         X2              =   11760
         Y1              =   0
         Y2              =   0
      End
   End
   Begin VB.Frame frmfiltros 
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      Caption         =   "Filtros"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   120
      TabIndex        =   23
      Top             =   2400
      Width           =   11775
      Begin VB.OptionButton OBcodigo 
         BackColor       =   &H00C0C0C0&
         Caption         =   "Filtrar por codigo"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2160
         TabIndex        =   36
         Top             =   480
         Width           =   1935
      End
      Begin VB.OptionButton Obnombre 
         BackColor       =   &H00C0C0C0&
         Caption         =   "Filtrar por n�mbre"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2160
         TabIndex        =   28
         Top             =   120
         Value           =   -1  'True
         Width           =   1935
      End
      Begin VB.TextBox txtbusqueda 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   120
         TabIndex        =   27
         Top             =   120
         Width           =   1935
      End
      Begin VB.ComboBox cmborden 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         ItemData        =   "mnabmclientes.frx":6FD6
         Left            =   9000
         List            =   "mnabmclientes.frx":6FD8
         TabIndex        =   25
         Text            =   "Nombre"
         Top             =   240
         Width           =   2655
      End
      Begin VB.CheckBox Checkgrilla 
         BackColor       =   &H00C0C0C0&
         Caption         =   "Activar Grilla"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   5160
         TabIndex        =   24
         Top             =   240
         Width           =   1575
      End
      Begin VB.Line Line14 
         BorderColor     =   &H80000007&
         X1              =   11760
         X2              =   11760
         Y1              =   0
         Y2              =   840
      End
      Begin VB.Line Line13 
         X1              =   0
         X2              =   0
         Y1              =   0
         Y2              =   840
      End
      Begin VB.Line Line12 
         BorderColor     =   &H80000007&
         X1              =   11760
         X2              =   0
         Y1              =   0
         Y2              =   0
      End
      Begin VB.Line Line11 
         BorderColor     =   &H80000007&
         X1              =   0
         X2              =   11880
         Y1              =   840
         Y2              =   840
      End
      Begin VB.Line Line2 
         BorderColor     =   &H80000007&
         X1              =   7680
         X2              =   7680
         Y1              =   0
         Y2              =   830
      End
      Begin VB.Line Line1 
         BorderColor     =   &H80000007&
         X1              =   4200
         X2              =   4200
         Y1              =   0
         Y2              =   830
      End
      Begin VB.Label lblingresenombre 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Ordenar por:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   0
         Left            =   7800
         TabIndex        =   26
         Top             =   240
         Width           =   1080
      End
   End
   Begin MSHierarchicalFlexGridLib.MSHFlexGrid MSHFlexGrid1 
      Height          =   30
      Left            =   2280
      TabIndex        =   8
      Top             =   5040
      Width           =   150
      _ExtentX        =   265
      _ExtentY        =   53
      _Version        =   393216
      _NumberOfBands  =   1
      _Band(0).Cols   =   2
   End
End
Attribute VB_Name = "mnabmclientes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim EstadoCliente As String
Dim Variable_orden As String
Dim variable_contador_validacion As Integer
Public Sub cmdnuevo_Click()
Bloqueo
Limpiar_Campos
txtnombre.SetFocus
EstadoCliente = "Nuevo"
cn.Open
Set rs = cn.Execute("Select max(idcliente) as maxid from Cliente") 'generador de idcliente
txtcodigo.Text = rs!maxid + 1
rs.Close
Set rs = Nothing
cn.Close
End Sub
Private Sub cmdmodificar_Click()
If txtnombre.Text = "" Or cmblocalidad.Text = "[Seleccione una localidad]" Then
MsgBox ("No se pueden realizar modificaciones sin seleccionar datos")
Else
Bloqueo
txtnombre.SetFocus
EstadoCliente = "Modificar"
End If
End Sub
Private Sub cmdeliminar_Click()
If txtnombre.Text = "" Or cmblocalidad.Text = "[Seleccione una localidad]" Then
MsgBox ("No se pueden eliminar clientes sin seleccionar datos")
Else
a = MsgBox("Esta Seguro que desea eliminarlo?", vbYesNo + vbInformation, "Eliminar")
If a = vbYes Then
cn.Open
cn.Execute ("Eliminar_Cliente " & txtcodigo.Text & "")
cn.Close
ActualizarLista
Limpiar_Campos
End If
End If
End Sub
Private Sub cmdguardar_Click()
Validacion
If variable_contador_validacion <> 5 Then
    MsgBox ("Complete los campos marcados")
Else
    If EstadoCliente = "Nuevo" Then 'almacenado en la base de datos
        cn.Open
        cn.Execute ("Nuevo_Cliente '" & txtnombre.Text & "','" & txtdomicilio.Text & "','" & txttelefono.Text & "','" & txtcuil.Text & "'," & cmblocalidad.ItemData(cmblocalidad.ListIndex) & "," & cmbiva.ItemData(cmbiva.ListIndex) & ",'" & txtemail.Text & "' ")
        cn.Close
    Else
        cn.Open
        cn.Execute ("Modificar_Cliente " & txtcodigo.Text & ",'" & txtnombre.Text & "','" & txtdomicilio.Text & "','" & txttelefono.Text & "', '" & txtcuil.Text & "'," & cmbiva.ItemData(cmbiva.ListIndex) & "," & cmblocalidad.ItemData(cmblocalidad.ListIndex) & ",'" & txtemail.Text & "'")
        cn.Close
    End If
    If Formulario = "facturacion" Then
    Formulario = "nada"
    funcion_facturacion
    Else
    cmdnuevo.Enabled = True
    cmdmodificar.Enabled = True
    cmdguardar.Enabled = False
    cmdeliminar.Enabled = True
    cmdcancelar.Enabled = False
    txtnombre.Enabled = False
    txtdomicilio.Enabled = False
    txttelefono.Enabled = False
    txtcuil.Enabled = False
    cmblocalidad.Enabled = False
    cmbiva.Enabled = False
    txtemail.Enabled = False
    cmborden.Enabled = True
    frmfiltros.Enabled = True
    FGClientes.Enabled = True
    lblvnombre.Visible = False
    lblvdomicilio.Visible = False
    lblvtelefono.Visible = False
    lblvlocalidad.Visible = False
    lblvcategoria.Visible = False
    Tama�o_Grilla
    ActualizarLista
    cmdnuevo.SetFocus
    End If
End If
End Sub
Private Sub cmdagregarlocalidad_Click()
frm_Agregar_Localidad_Rubro.Localidad
frm_Agregar_Localidad_Rubro.Show
End Sub
Private Sub cmdcancelar_Click()
cmdnuevo.Enabled = True
cmdmodificar.Enabled = True
cmdeliminar.Enabled = True
cmdguardar.Enabled = False
cmdcancelar.Enabled = False
txtnombre.Enabled = False
txtdomicilio.Enabled = False
txttelefono.Enabled = False
txtcuil.Enabled = False
cmblocalidad.Enabled = False
cmbiva.Enabled = False
txtemail.Enabled = False
frmfiltros.Enabled = True
FGClientes.Enabled = True
Limpiar_Campos
lblvnombre.Visible = False
lblvdomicilio.Visible = False
lblvtelefono.Visible = False
lblvlocalidad.Visible = False
lblvcategoria.Visible = False
End Sub
Private Sub cmdsalir_Click()
Unload Me
End Sub
Private Sub Checkgrilla_Click()
ActualizarLista
cmborden.Text = "Nombre"
End Sub
Private Sub txtbusqueda_Change() 'busqueda de cliente
FGClientes.Rows = 1
funcion_Busqueda_cuadro
End Sub
Private Sub FGClientes_RowColChange()
funcion_Busqueda_grilla
End Sub
Private Sub cmborden_click()
If cmborden.Text = "Nombre" Or cmborden.Text = "Codigo" Or cmborden.Text = "Domicilio" Or cmborden.Text = "Telefono" Or cmborden.Text = "Cuit" Or cmborden.Text = "Email" Or cmborden.Text = "Categor�a" Or cmborden.Text = "Localidad" Then
    If cmborden.Text = "Codigo" Or cmborden.Text = "Categor�a" Then
        If cmborden.Text = "Codigo" Then
        Variable_orden = "idcliente"
        End If
        If cmborden.Text = "Categor�a" Then
        varible_orden = "categoria"
        End If
    Else
        Variable_orden = cmborden.Text
    End If
FGClientes.Rows = 1
If Checkgrilla.Value = 1 Then
actualizarlista_filtros
End If
End If
End Sub
Private Sub txtnombre_KeyPress(KeyAscii As Integer)
strvalid = "QWERTYUIOPASDFGHJKL�ZXCVBNM�����qwertyuiopasdfghjklzxcvbnm������ "
KeyAscii = Asc(Chr(KeyAscii))
If (KeyAscii > 10) Then
If InStr(strvalid, Chr(KeyAscii)) = 0 Then
KeyAscii = 0
End If
End If
End Sub
Private Sub txttelefono_KeyPress(KeyAscii As Integer)
strvalid = "1234567890"
KeyAscii = Asc(Chr(KeyAscii))
If (KeyAscii > 10) Then
If InStr(strvalid, Chr(KeyAscii)) = 0 Then
KeyAscii = 0
End If
End If
End Sub
Private Sub txtcuil_KeyPress(KeyAscii As Integer)
strvalid = "1234567890"
KeyAscii = Asc(Chr(KeyAscii))
If (KeyAscii > 10) Then
If InStr(strvalid, Chr(KeyAscii)) = 0 Then
KeyAscii = 0
End If
End If
End Sub
Private Sub Form_Load()
Variable_orden = "Nombre"
Tama�o_Grilla
funcion_orden
ActualizarLista
CondicionIva
Localidad
End Sub
Sub Tama�o_Grilla() 'declaracion de grilla
With FGClientes
 'cantidad de columnas
.Cols = 8
 'ancho de las columnas
.ColWidth(0) = 700
.ColWidth(1) = 1700
.ColWidth(2) = 1700
.ColWidth(3) = 1150
.ColWidth(4) = 1150
.ColWidth(5) = 1300
.ColWidth(6) = 1750
.ColWidth(7) = 2055
.TextArray(0) = "Codigo"
.TextArray(1) = "Apellido y Nombre"
.TextArray(2) = "Domicilio"
.TextArray(3) = "Telefono"
.TextArray(4) = "Cuit/DNI"
.TextArray(5) = "Localidad"
.TextArray(6) = "Categor�a"
.TextArray(7) = "Correo"
End With
End Sub
Sub ActualizarLista()
For i = 0 To 7
FGClientes.Col = i
FGClientes.CellBackColor = &H8000000F
Next i
FGClientes.Rows = 1
If Checkgrilla.Value = 1 Then
cn.Open
Set rs = cn.Execute("Listado_Clientes")
Do While rs.EOF = False
With FGClientes
.Rows = .Rows + 1
.TextMatrix(.Rows - 1, 0) = rs!idcliente
.TextMatrix(.Rows - 1, 1) = rs!nombre
.TextMatrix(.Rows - 1, 2) = rs!domicilio
.TextMatrix(.Rows - 1, 3) = rs!telefono
.TextMatrix(.Rows - 1, 4) = rs!cuit
.TextMatrix(.Rows - 1, 5) = rs!Localidad
.TextMatrix(.Rows - 1, 6) = rs!categoria
.TextMatrix(.Rows - 1, 7) = rs!email
rs.MoveNext
End With
Loop
rs.Close
Set rs = Nothing
cn.Close
End If
End Sub
Sub actualizarlista_filtros()
cn.Open
Set rs = cn.Execute("SELECT Cliente.idcliente, Cliente.nombre, Cliente.domicilio, Cliente.telefono, Cliente.cuit, localidad.localidad, Cliente.email, Categoria.categoria FROM Cliente INNER JOIN Categoria ON Cliente.idcategoria = Categoria.idcategoria INNER JOIN localidad ON Cliente.idlocalidad = localidad.idlocalidad ORDER BY " & Variable_orden & " ASC")
With FGClientes
.Rows = 1
Do While rs.EOF = False
.Rows = .Rows + 1
.TextMatrix(.Rows - 1, 0) = rs!idcliente
.TextMatrix(.Rows - 1, 1) = rs!nombre
.TextMatrix(.Rows - 1, 2) = rs!domicilio
.TextMatrix(.Rows - 1, 3) = rs!telefono
.TextMatrix(.Rows - 1, 4) = rs!cuit
.TextMatrix(.Rows - 1, 5) = rs!Localidad
.TextMatrix(.Rows - 1, 6) = rs!categoria
.TextMatrix(.Rows - 1, 7) = rs!email
rs.MoveNext
Loop
End With
rs.Close
Set rs = Nothing
cn.Close
End Sub
Sub CondicionIva() 'Combobox categoria de iva
cn.Open
Set rs = cn.Execute("Listado_Categoria")
Do While rs.EOF = False
cmbiva.AddItem rs!categoria
cmbiva.ItemData(cmbiva.NewIndex) = rs!idcategoria
rs.MoveNext
Loop
rs.Close
Set rs = Nothing
cn.Close
End Sub
Sub funcion_Busqueda_cuadro()
FGClientes.Rows = 1
For i = 0 To 7
FGClientes.Col = i
FGClientes.CellBackColor = &H8000000F
Next i
cn.Open
If Obnombre.Value = True Then
Set rs = cn.Execute("Select  Cliente.idcliente, Cliente.nombre, Cliente.domicilio, Cliente.telefono, Cliente.cuit, Cliente.idlocalidad, Cliente.idcategoria, Cliente.email, Categoria.categoria, Localidad.localidad FROM Cliente INNER JOIN Categoria ON Cliente.idcategoria = Categoria.idcategoria INNER JOIN localidad ON Cliente.idlocalidad = localidad.idlocalidad where nombre like" & " '" & "" & txtbusqueda.Text & "%' ORDER By " & Variable_orden & " ASC")
Else
Set rs = cn.Execute("Select  Cliente.idcliente, Cliente.nombre, Cliente.domicilio, Cliente.telefono, Cliente.cuit, Cliente.idlocalidad, Cliente.idcategoria, Cliente.email, Categoria.categoria, Localidad.localidad FROM Cliente INNER JOIN Categoria ON Cliente.idcategoria = Categoria.idcategoria INNER JOIN localidad ON Cliente.idlocalidad = localidad.idlocalidad where idcliente like" & " '" & "" & txtbusqueda.Text & "%' ORDER By " & Variable_orden & " ASC")
End If
Do While rs.EOF = False
If Checkgrilla.Value = 1 Then
With FGClientes
.Rows = .Rows + 1
.TextMatrix(.Rows - 1, 0) = rs!idcliente
.TextMatrix(.Rows - 1, 1) = rs!nombre
.TextMatrix(.Rows - 1, 2) = rs!domicilio
.TextMatrix(.Rows - 1, 3) = rs!telefono
.TextMatrix(.Rows - 1, 4) = rs!cuit
.TextMatrix(.Rows - 1, 5) = rs!Localidad
.TextMatrix(.Rows - 1, 6) = rs!categoria
.TextMatrix(.Rows - 1, 7) = rs!email
End With
End If
rs.MoveNext
Loop
If Obnombre.Value = True Then
Set rs = cn.Execute("Select  Cliente.idcliente, Cliente.nombre, Cliente.domicilio, Cliente.telefono, Cliente.cuit, Cliente.idlocalidad, Cliente.idcategoria, Cliente.email, Categoria.categoria, Localidad.localidad FROM Cliente INNER JOIN Categoria ON Cliente.idcategoria = Categoria.idcategoria INNER JOIN localidad ON Cliente.idlocalidad = localidad.idlocalidad where nombre like" & " '" & "" & txtbusqueda.Text & "%' ORDER By nombre ASC")
Else
Set rs = cn.Execute("Select  Cliente.idcliente, Cliente.nombre, Cliente.domicilio, Cliente.telefono, Cliente.cuit, Cliente.idlocalidad, Cliente.idcategoria, Cliente.email, Categoria.categoria, Localidad.localidad FROM Cliente INNER JOIN Categoria ON Cliente.idcategoria = Categoria.idcategoria INNER JOIN localidad ON Cliente.idlocalidad = localidad.idlocalidad where idcliente like" & " '" & "" & txtbusqueda.Text & "%' ORDER By idcliente ASC")
End If
If rs.EOF = False Then
txtcodigo.Text = rs!idcliente
txtnombre.Text = rs!nombre
txtdomicilio.Text = rs!domicilio
txttelefono.Text = rs!telefono
txtcuil.Text = rs!cuit
txtemail.Text = rs!email
cmbiva.ListIndex = rs!idcategoria - 1
cmblocalidad.ListIndex = rs!idLocalidad - 1
End If
rs.Close
Set rs = Nothing
cn.Close
If txtbusqueda.Text = "" Then
Limpiar_Campos
End If
End Sub
Sub funcion_Busqueda_grilla()
cn.Open
Set rs = cn.Execute("Select idcliente, nombre, domicilio, telefono, cuit, idlocalidad, idcategoria, email FROM Cliente where nombre = '" & FGClientes.TextMatrix(FGClientes.Row, 1) & "'")
Do While rs.EOF = False
txtcodigo.Text = rs!idcliente
txtnombre.Text = rs!nombre
txtdomicilio.Text = rs!domicilio
txttelefono.Text = rs!telefono
txtcuil.Text = rs!cuit
txtemail.Text = rs!email
cmbiva.ListIndex = rs!idcategoria - 1
cmblocalidad.ListIndex = rs!idLocalidad - 1
rs.MoveNext
Loop
rs.Close
Set rs = Nothing
cn.Close
End Sub
Sub Localidad() 'Combobox Localidad
cn.Open
Set rs = cn.Execute("Listado_Localidad")
Do While rs.EOF = False
cmblocalidad.AddItem rs!Localidad
cmblocalidad.ItemData(cmblocalidad.NewIndex) = rs!idLocalidad
rs.MoveNext
Loop
rs.Close
Set rs = Nothing
cn.Close
End Sub
Sub funcion_orden()
With cmborden
.AddItem "Codigo"
.AddItem "Nombre"
.AddItem "Domicilio"
.AddItem "Telefono"
.AddItem "Cuit"
.AddItem "Email"
.AddItem "Categor�a"
.AddItem "Localidad"
End With
End Sub
Sub Validacion()
variable_contador_validacion = 0
If txtnombre.Text = "" Then
lblvnombre.Visible = True
Else
lblvnombre.Visible = False
variable_contador_validacion = variable_contador_validacion + 1
End If
If txtdomicilio.Text = "" Then
lblvdomicilio.Visible = True
Else
lblvdomicilio.Visible = False
variable_contador_validacion = variable_contador_validacion + 1
End If
If txttelefono.Text = "" Then
lblvtelefono.Visible = True
Else
lblvtelefono.Visible = False
variable_contador_validacion = variable_contador_validacion + 1
End If

If cmblocalidad.ListIndex = -1 Then
lblvlocalidad.Visible = True
Else
lblvlocalidad.Visible = False
variable_contador_validacion = variable_contador_validacion + 1
End If

If cmbiva.ListIndex = -1 Then
lblvcategoria.Visible = True
Else
lblvcategoria.Visible = False
variable_contador_validacion = variable_contador_validacion + 1
End If
End Sub
Sub Limpiar_Campos()
txtcodigo.Text = ""
txtnombre.Text = ""
txtdomicilio.Text = ""
txttelefono.Text = ""
txtcuil.Text = ""
txtemail.Text = ""
cmbiva.Text = "[Seleccione una categor�a]"
cmblocalidad.Text = "[Seleccione una localidad]"
End Sub
Sub Bloqueo()
cmdnuevo.Enabled = False
cmdmodificar.Enabled = False
cmdeliminar.Enabled = False
cmdguardar.Enabled = True
cmdcancelar.Enabled = True
txtnombre.Enabled = True
txtdomicilio.Enabled = True
txttelefono.Enabled = True
txtcuil.Enabled = True
cmblocalidad.Enabled = True
cmbiva.Enabled = True
txtemail.Enabled = True
frmfiltros.Enabled = False
FGClientes.Enabled = False
End Sub
Sub funcion_facturacion() 'envia datos al formulario frm_facturacion
   With frm_facturacion
    .Estado_Inicial
    .lblcodigo = txtcodigo.Text
    .lblnombre = txtnombre.Text
    .lbldomicilio = txtdomicilio.Text
    .lblcuit = txtcuil.Text
    .lblcondicion = cmbiva.Text
    .lblsaldo = 0
    .lblsaldo.ForeColor = &HC000&
    If .lblcondicion.Caption = "Responsable Inscripto" Then
    .lbltipofactura.Caption = "A"
    Else
    .lbltipofactura.Caption = "B"
    End If
    cn.Open
    Set rs = cn.Execute("select id,facturas_a,facturas_b from Parametros")
    If .lbltipofactura.Caption = "A" Then
        .lblnfactura.Caption = "0001-" & Format(rs!facturas_a + 1, "00000000")
    Else
        .lblnfactura.Caption = "0001-" & Format(rs!facturas_b + 1, "00000000")
    End If
    rs.Close
    Set rs = Nothing
    cn.Close
    .Show
    .frmencabezado.Enabled = True
    .Borderusuario.BorderColor = &H0&
    .color_encabezado
    .cmbcomprobante.SetFocus
    End With
Unload Me
End Sub
