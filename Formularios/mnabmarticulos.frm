VERSION 5.00
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "mshflxgd.ocx"
Begin VB.Form mnabmarticulos 
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Articulos"
   ClientHeight    =   8040
   ClientLeft      =   945
   ClientTop       =   915
   ClientWidth     =   11040
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "mnabmarticulos.frx":0000
   ScaleHeight     =   8040
   ScaleWidth      =   11040
   Begin VB.Frame Frame4 
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      Caption         =   "Frame4"
      Height          =   4575
      Left            =   120
      TabIndex        =   34
      Top             =   3360
      Width           =   10815
      Begin MSHierarchicalFlexGridLib.MSHFlexGrid FGArticulos 
         Height          =   4335
         Left            =   120
         TabIndex        =   35
         Top             =   120
         Width           =   10575
         _ExtentX        =   18653
         _ExtentY        =   7646
         _Version        =   393216
         FixedCols       =   0
         ScrollBars      =   2
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _NumberOfBands  =   1
         _Band(0).Cols   =   2
      End
      Begin VB.Line Line18 
         BorderColor     =   &H80000007&
         X1              =   10800
         X2              =   10800
         Y1              =   0
         Y2              =   4560
      End
      Begin VB.Line Line17 
         BorderColor     =   &H80000007&
         X1              =   0
         X2              =   10920
         Y1              =   4560
         Y2              =   4560
      End
      Begin VB.Line Line16 
         BorderColor     =   &H80000007&
         X1              =   0
         X2              =   10800
         Y1              =   0
         Y2              =   0
      End
      Begin VB.Line Line15 
         BorderColor     =   &H80000007&
         X1              =   0
         X2              =   0
         Y1              =   0
         Y2              =   4560
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      Caption         =   "Detalles"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1455
      Left            =   120
      TabIndex        =   6
      Top             =   120
      Width           =   10815
      Begin VB.CommandButton cmdagregarrubro 
         Caption         =   "+"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   10080
         TabIndex        =   36
         Top             =   240
         Width           =   375
      End
      Begin VB.TextBox txtcodigo 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         Height          =   285
         Left            =   4800
         TabIndex        =   0
         Top             =   240
         Width           =   1695
      End
      Begin VB.TextBox txtdescripcion 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         Height          =   285
         Left            =   1440
         TabIndex        =   1
         Top             =   600
         Width           =   5055
      End
      Begin VB.TextBox txtid 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         Height          =   285
         Left            =   1440
         TabIndex        =   7
         Top             =   240
         Width           =   1695
      End
      Begin VB.TextBox txtpreciocosto 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         Height          =   285
         Left            =   1440
         TabIndex        =   2
         Top             =   960
         Width           =   1695
      End
      Begin VB.TextBox txtprecioventa 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         Height          =   285
         Left            =   4800
         TabIndex        =   3
         Top             =   960
         Width           =   1695
      End
      Begin VB.ComboBox cmbrubro 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   7680
         TabIndex        =   4
         Text            =   "[Seleccione un rubro]"
         Top             =   240
         Width           =   2295
      End
      Begin VB.TextBox txtstock 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         Height          =   285
         Left            =   7680
         TabIndex        =   5
         Top             =   960
         Width           =   1695
      End
      Begin VB.Line Line6 
         X1              =   10800
         X2              =   10800
         Y1              =   0
         Y2              =   1440
      End
      Begin VB.Line Line5 
         X1              =   0
         X2              =   0
         Y1              =   0
         Y2              =   1440
      End
      Begin VB.Line Line4 
         X1              =   0
         X2              =   10920
         Y1              =   1440
         Y2              =   1440
      End
      Begin VB.Line Line3 
         X1              =   0
         X2              =   10800
         Y1              =   0
         Y2              =   0
      End
      Begin VB.Label lblvdescripcion 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "*"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   21.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   495
         Left            =   6600
         TabIndex        =   20
         Top             =   600
         Visible         =   0   'False
         Width           =   165
      End
      Begin VB.Label lblvpreciocosto 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "*"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   21.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   495
         Left            =   3240
         TabIndex        =   19
         Top             =   960
         Visible         =   0   'False
         Width           =   165
      End
      Begin VB.Label lblvprecioventa 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "*"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   21.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   495
         Left            =   6600
         TabIndex        =   18
         Top             =   960
         Visible         =   0   'False
         Width           =   165
      End
      Begin VB.Label lblvstock 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "*"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   21.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   495
         Left            =   9480
         TabIndex        =   17
         Top             =   960
         Visible         =   0   'False
         Width           =   165
      End
      Begin VB.Label lblvrubro 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "*"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   21.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   495
         Left            =   10560
         TabIndex        =   16
         Top             =   240
         Visible         =   0   'False
         Width           =   165
      End
      Begin VB.Label lblvcodigo 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "*"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   21.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   495
         Left            =   6600
         TabIndex        =   15
         Top             =   240
         Visible         =   0   'False
         Width           =   165
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Codigo"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   3960
         TabIndex        =   14
         Top             =   240
         Width           =   600
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Descripci�n"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   240
         TabIndex        =   13
         Top             =   600
         Width           =   1020
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Rubro"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   7080
         TabIndex        =   12
         Top             =   240
         Width           =   510
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Precio costo"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   240
         TabIndex        =   11
         Top             =   960
         Width           =   1095
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "ID"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   240
         TabIndex        =   10
         Top             =   240
         Width           =   180
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Precio venta"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   3600
         TabIndex        =   9
         Top             =   960
         Width           =   1065
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Stock"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   7080
         TabIndex        =   8
         Top             =   960
         Width           =   510
      End
   End
   Begin VB.Frame Frame3 
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      Caption         =   "Control"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   120
      TabIndex        =   28
      Top             =   1680
      Width           =   10815
      Begin VB.CommandButton cmdimpexp 
         Caption         =   "Importar/Exportar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   6960
         TabIndex        =   37
         Top             =   120
         Width           =   2055
      End
      Begin VB.CommandButton cmdsalir 
         Caption         =   "Salir"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   9120
         TabIndex        =   33
         Top             =   120
         Width           =   1575
      End
      Begin VB.CommandButton cmdcancelar 
         Caption         =   "Cancelar"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   5280
         TabIndex        =   32
         Top             =   120
         Width           =   1575
      End
      Begin VB.CommandButton cmdguardar 
         Caption         =   "Guardar"
         CausesValidation=   0   'False
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3600
         TabIndex        =   31
         Top             =   120
         Width           =   1575
      End
      Begin VB.CommandButton cmdmodificar 
         Caption         =   "Modificar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1920
         TabIndex        =   30
         Top             =   120
         Width           =   1575
      End
      Begin VB.CommandButton cmdnuevo 
         Caption         =   "Nuevo"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   240
         TabIndex        =   29
         Top             =   120
         Width           =   1575
      End
      Begin VB.Line Line10 
         BorderColor     =   &H80000007&
         X1              =   10800
         X2              =   10800
         Y1              =   0
         Y2              =   600
      End
      Begin VB.Line Line9 
         BorderColor     =   &H80000007&
         X1              =   0
         X2              =   0
         Y1              =   0
         Y2              =   600
      End
      Begin VB.Line Line8 
         BorderColor     =   &H80000007&
         X1              =   0
         X2              =   10920
         Y1              =   600
         Y2              =   600
      End
      Begin VB.Line Line7 
         BorderColor     =   &H80000007&
         X1              =   0
         X2              =   10800
         Y1              =   0
         Y2              =   0
      End
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      Caption         =   "Filtros"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   120
      TabIndex        =   21
      Top             =   2400
      Width           =   10815
      Begin VB.TextBox txtbuscarnombre 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   120
         TabIndex        =   26
         Top             =   120
         Width           =   1695
      End
      Begin VB.ComboBox cmborden 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   9120
         TabIndex        =   25
         Text            =   "Descripci�n"
         Top             =   240
         Width           =   1575
      End
      Begin VB.CheckBox Checkgrilla 
         BackColor       =   &H00C0C0C0&
         Caption         =   "Activar Grilla"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   5280
         TabIndex        =   24
         Top             =   240
         Width           =   1575
      End
      Begin VB.OptionButton Obdescripcion 
         BackColor       =   &H00C0C0C0&
         Caption         =   "Filtrar por descripci�n"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   1920
         TabIndex        =   23
         Top             =   120
         Value           =   -1  'True
         Width           =   2175
      End
      Begin VB.OptionButton OBcodigo 
         BackColor       =   &H00C0C0C0&
         Caption         =   "Filtrar por codigo"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   1920
         TabIndex        =   22
         Top             =   480
         Width           =   1935
      End
      Begin VB.Line Line14 
         BorderColor     =   &H80000007&
         X1              =   10800
         X2              =   10800
         Y1              =   0
         Y2              =   840
      End
      Begin VB.Line Line13 
         BorderColor     =   &H80000007&
         X1              =   0
         X2              =   0
         Y1              =   0
         Y2              =   840
      End
      Begin VB.Line Line12 
         BorderColor     =   &H80000007&
         X1              =   0
         X2              =   10920
         Y1              =   840
         Y2              =   840
      End
      Begin VB.Line Line11 
         BorderColor     =   &H80000007&
         X1              =   0
         X2              =   10800
         Y1              =   0
         Y2              =   0
      End
      Begin VB.Line Line2 
         BorderColor     =   &H80000007&
         X1              =   7800
         X2              =   7800
         Y1              =   15
         Y2              =   1085
      End
      Begin VB.Label Label8 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Ordenar por:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   7920
         TabIndex        =   27
         Top             =   240
         Width           =   1080
      End
      Begin VB.Line Line1 
         BorderColor     =   &H80000007&
         X1              =   4200
         X2              =   4200
         Y1              =   15
         Y2              =   1085
      End
   End
End
Attribute VB_Name = "mnabmarticulos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim EstadoArticulo As String
Dim Variable_orden As String
Dim variable_contador_validacion As Integer
Dim Alerta As String
Private Sub cmdagregarrubro_Click()
frm_Agregar_Localidad_Rubro.Rubro
frm_Agregar_Localidad_Rubro.Show
End Sub
Private Sub cmdimpexp_Click()
frm_imp_exp.Show
End Sub
Private Sub cmdnuevo_Click()
cmdnuevo.Enabled = False
cmdmodificar.Enabled = False
cmdguardar.Enabled = True
cmdcancelar.Enabled = True
txtcodigo.Enabled = True
txtdescripcion.Enabled = True
cmbrubro.Enabled = True
txtpreciocosto.Enabled = True
txtprecioventa.Enabled = True
txtstock.Enabled = True
cmborden.Enabled = False
txtbuscarnombre.Enabled = False
FGArticulos.Enabled = False
txtcodigo.Text = ""
txtdescripcion.Text = ""
cmbrubro.Text = "[Seleccione un rubro]"
txtpreciocosto.Text = ""
txtprecioventa.Text = ""
txtstock.Text = ""
txtcodigo.SetFocus
EstadoArticulo = "Nuevo"
cn.Open
Set rs = cn.Execute("Select max(idarticulos) as maxid from articulos")
txtid.Text = rs!maxid + 1
rs.Close
Set rs = Nothing
cn.Close
End Sub
Private Sub cmdmodificar_Click()
If txtdescripcion.Text = "" Or cmbrubro.Text = "[Seleccione un rubro]" Then
MsgBox ("No se pueden realizar modificaciones sin seleccionar datos")
Else
cmdnuevo.Enabled = False
cmdmodificar.Enabled = False
cmdguardar.Enabled = True
cmdcancelar.Enabled = True
txtcodigo.Enabled = True
txtdescripcion.Enabled = True
cmbrubro.Enabled = True
txtpreciocosto.Enabled = True
txtprecioventa.Enabled = True
txtstock.Enabled = True
cmborden.Enabled = False
txtbuscarnombre.Enabled = False
FGArticulos.Enabled = False
txtdescripcion.SetFocus
EstadoArticulo = "Modificar"
End If
End Sub
Private Sub cmdguardar_Click()
funcion_Validacion
If variable_contador_validacion <> 6 Then
    MsgBox ("Complete los campos marcados")
Else
    If EstadoArticulo = "Nuevo" Then
        cn.Open
        cn.Execute ("Nuevo_Articulo '" & txtcodigo.Text & "','" & txtdescripcion.Text & "'," & cmbrubro.ItemData(cmbrubro.ListIndex) & "," & txtstock.Text & "," & Trim(Str(txtpreciocosto.Text)) & "," & Trim(Str(txtprecioventa.Text)) & "")
        cn.Close
    Else
        cn.Open
        cn.Execute ("Modificar_Articulo " & txtid.Text & ",'" & txtcodigo.Text & "','" & txtdescripcion.Text & "'," & cmbrubro.ItemData(cmbrubro.ListIndex) & "," & txtstock.Text & "," & Trim(Str(txtpreciocosto.Text)) & "," & Trim(Str(txtprecioventa.Text)) & "")
        cn.Close
    End If
    cmdnuevo.Enabled = True
    cmdmodificar.Enabled = True
    cmdguardar.Enabled = False
    cmdcancelar.Enabled = False
    txtcodigo.Enabled = False
    txtdescripcion.Enabled = False
    cmbrubro.Enabled = False
    txtpreciocosto.Enabled = False
    txtprecioventa.Enabled = False
    txtstock.Enabled = False
    cmborden.Enabled = True
    txtbuscarnombre.Enabled = True
    FGArticulos.Enabled = True
    Tama�o_Grilla
    Actualizar_Grilla
End If
End Sub
Private Sub cmdcancelar_Click()
cmdnuevo.Enabled = True
cmdmodificar.Enabled = True
cmdguardar.Enabled = False
cmdcancelar.Enabled = False
txtcodigo.Enabled = False
txtdescripcion.Enabled = False
cmbrubro.Enabled = False
txtpreciocosto.Enabled = False
txtprecioventa.Enabled = False
txtstock.Enabled = False
cmborden.Enabled = True
txtbuscarnombre.Enabled = True
FGArticulos.Enabled = True
txtid.Text = " "
txtcodigo.Text = " "
txtdescripcion.Text = " "
cmbrubro.Text = "[Seleccione un rubro]"
txtpreciocosto.Text = " "
txtprecioventa.Text = " "
txtstock.Text = " "
lblvcodigo.Visible = False
lblvdescripcion.Visible = False
lblvpreciocosto.Visible = False
lblvprecioventa.Visible = False
lblvrubro.Visible = False
lblvstock.Visible = False
End Sub
Private Sub cmdsalir_Click()
Unload Me
End Sub
Private Sub Checkgrilla_Click()
Actualizar_Grilla
cmborden.Text = "Descripci�n"
End Sub
Private Sub FGArticulos_RowColChange()
funcion_Busqueda_grilla
End Sub
Private Sub txtbuscarnombre_Change()
Tama�o_Grilla
funcion_Busqueda_cuadro
End Sub
Private Sub cmborden_click()
If cmborden.Text = "ID" Or cmborden.Text = "Precio costo" Or cmborden.Text = "Precio venta" Or cmborden.Text = "Descripci�n" Then
    If cmborden.Text = "Descripci�n" Then
        Variable_orden = "descripcion"
    End If
    If cmborden.Text = "ID" Then
        Variable_orden = "idarticulos"
    End If
    If cmborden.Text = "Precio costo" Then
        Variable_orden = "precio_costo"
    End If
    If cmborden.Text = "Precio venta" Then
        Variable_orden = "precio_venta"
    End If
Else
    Variable_orden = cmborden.Text
End If
Tama�o_Grilla
If Checkgrilla.Value = 1 Then
ActualizarGrilla_orden
End If
End Sub
Private Sub txtdescripcion_KeyPress(KeyAscii As Integer)
strvalid = "QWERTYUIOPASDFGHJKL�ZXCVBNM�����qwertyuiopasdfghjklzxcvbnm������0123456789' "
KeyAscii = Asc(Chr(KeyAscii))
If (KeyAscii > 10) Then
If InStr(strvalid, Chr(KeyAscii)) = 0 Then
KeyAscii = 0
End If
End If
End Sub
Private Sub txtcodigo_KeyPress(KeyAscii As Integer)
strvalid = "1234567890 "
KeyAscii = Asc(Chr(KeyAscii))
If (KeyAscii > 10) Then
If InStr(strvalid, Chr(KeyAscii)) = 0 Then
KeyAscii = 0
End If
End If
End Sub
Private Sub txtpreciocosto_KeyPress(KeyAscii As Integer)
strvalid = "1234567890, "
KeyAscii = Asc(Chr(KeyAscii))
If (KeyAscii > 10) Then
If InStr(strvalid, Chr(KeyAscii)) = 0 Then
KeyAscii = 0
End If
End If
End Sub
Private Sub txtprecioventa_KeyPress(KeyAscii As Integer)
strvalid = "1234567890, "
KeyAscii = Asc(Chr(KeyAscii))
If (KeyAscii > 10) Then
If InStr(strvalid, Chr(KeyAscii)) = 0 Then
KeyAscii = 0
End If
End If
End Sub
Private Sub txtstock_KeyPress(KeyAscii As Integer)
strvalid = "1234567890. "
KeyAscii = Asc(Chr(KeyAscii))
If (KeyAscii > 10) Then
If InStr(strvalid, Chr(KeyAscii)) = 0 Then
KeyAscii = 0
End If
End If
End Sub
Private Sub Form_Load()
Variable_orden = "Descripcion"
funcion_orden
Tama�o_Grilla
Actualizar_Grilla
Rubro
Alerta = "libre"
End Sub
Sub Tama�o_Grilla()
With FGArticulos
.Cols = 7
.ColWidth(0) = 700
.ColWidth(1) = 1000
.ColWidth(2) = 3930
.ColWidth(3) = 550
.ColWidth(4) = 1000
.ColWidth(5) = 1500
.ColWidth(6) = 1565
.ColAlignment(5) = flexAlignRightCenter
.ColAlignment(6) = flexAlignRightCenter
.TextArray(0) = "ID"
.TextArray(1) = "Codigo"
.TextArray(2) = "Descripci�n"
.TextArray(3) = "Stock"
.TextArray(4) = "Rubro"
.TextArray(5) = "Precio de compra"
.TextArray(6) = "Precio de venta"
End With
End Sub
Sub Actualizar_Grilla()
For i = 0 To 6
FGArticulos.Col = i
FGArticulos.CellBackColor = &H8000000F
Next i
FGArticulos.Rows = 1
If Checkgrilla.Value = 1 Then
cn.Open
Set rs = cn.Execute("Listado_Articulos")
With FGArticulos
Do While rs.EOF = False
.Rows = .Rows + 1
.TextMatrix(.Rows - 1, 0) = rs!idarticulos
.TextMatrix(.Rows - 1, 1) = rs!codigo
.TextMatrix(.Rows - 1, 2) = rs!descripcion
.TextMatrix(.Rows - 1, 3) = rs!stock
.TextMatrix(.Rows - 1, 4) = rs!Rubro
.TextMatrix(.Rows - 1, 5) = rs!precio_costo
.TextMatrix(.Rows - 1, 6) = rs!precio_venta
rs.MoveNext
Loop
End With
rs.Close
Set rs = Nothing
cn.Close
End If
Stock_Bajo
End Sub
Sub ActualizarGrilla_orden()
cn.Open
Set rs = cn.Execute("SELECT Articulos.idarticulos, Articulos.codigo, Articulos.descripcion, Articulos.precio_venta, Articulos.precio_costo, Articulos.stock, Rubro.rubro FROM Articulos INNER JOIN Rubro ON Articulos.idrubro = Rubro.idrubro  ORDER By " & Variable_orden & " ASC")
FGArticulos.Rows = 1
With FGArticulos
Do While rs.EOF = False
.Rows = .Rows + 1
.TextMatrix(.Rows - 1, 0) = rs!idarticulos
.TextMatrix(.Rows - 1, 1) = rs!codigo
.TextMatrix(.Rows - 1, 2) = rs!descripcion
.TextMatrix(.Rows - 1, 3) = rs!stock
.TextMatrix(.Rows - 1, 4) = rs!Rubro
.TextMatrix(.Rows - 1, 5) = rs!precio_costo
.TextMatrix(.Rows - 1, 6) = rs!precio_venta
rs.MoveNext
Loop
End With
rs.Close
Set rs = Nothing
cn.Close
Stock_Bajo
End Sub
Sub funcion_Busqueda_cuadro()
For i = 0 To 6
FGArticulos.Col = i
FGArticulos.CellBackColor = &H8000000F
Next i
FGArticulos.Rows = 1
cn.Open
If OBdescripcion.Value = True Then
Set rs = cn.Execute("Select  Articulos.idarticulos, Articulos.codigo, Articulos.descripcion, Articulos.precio_venta, Articulos.precio_costo, Articulos.stock, Articulos.idrubro, Rubro.rubro FROM Articulos INNER JOIN Rubro ON Articulos.idrubro = Rubro.idrubro where descripcion like" & " '" & "%" & txtbuscarnombre.Text & "%' ORDER By " & Variable_orden & " ASC")
Else
Set rs = cn.Execute("Select  Articulos.idarticulos, Articulos.codigo, Articulos.descripcion, Articulos.precio_venta, Articulos.precio_costo, Articulos.stock, Articulos.idrubro, Rubro.rubro FROM Articulos INNER JOIN Rubro ON Articulos.idrubro = Rubro.idrubro where codigo like" & " '" & "%" & txtbuscarnombre.Text & "%' ORDER By " & Variable_orden & " ASC")
End If
Do While rs.EOF = False
With FGArticulos
If Checkgrilla.Value = 1 Then
.Rows = .Rows + 1
.TextMatrix(.Rows - 1, 0) = rs!idarticulos
.TextMatrix(.Rows - 1, 1) = rs!codigo
.TextMatrix(.Rows - 1, 2) = rs!descripcion
.TextMatrix(.Rows - 1, 3) = rs!stock
.TextMatrix(.Rows - 1, 4) = rs!Rubro
.TextMatrix(.Rows - 1, 5) = rs!precio_costo
.TextMatrix(.Rows - 1, 6) = rs!precio_venta
End If
End With
rs.MoveNext
Loop
If OBdescripcion.Value = True Then
Set rs = cn.Execute("Select  Articulos.idarticulos, Articulos.codigo, Articulos.descripcion, Articulos.precio_venta, Articulos.precio_costo, Articulos.stock, Articulos.idrubro, Rubro.rubro FROM Articulos INNER JOIN Rubro ON Articulos.idrubro = Rubro.idrubro where descripcion like" & " '" & "%" & txtbuscarnombre.Text & "%' ORDER By descripcion ASC")
Else
Set rs = cn.Execute("Select  Articulos.idarticulos, Articulos.codigo, Articulos.descripcion, Articulos.precio_venta, Articulos.precio_costo, Articulos.stock, Articulos.idrubro, Rubro.rubro FROM Articulos INNER JOIN Rubro ON Articulos.idrubro = Rubro.idrubro where codigo like" & " '" & "%" & txtbuscarnombre.Text & "%' ORDER By codigo ASC")
End If
If rs.EOF = False Then
txtid.Text = rs!idarticulos
txtcodigo.Text = rs!codigo
txtdescripcion.Text = rs!descripcion
txtprecioventa.Text = rs!precio_venta
txtpreciocosto.Text = rs!precio_costo
txtstock.Text = rs!stock
cmbrubro.ListIndex = rs!idrubro - 1
End If
rs.Close
Set rs = Nothing
cn.Close
If txtbuscarnombre.Text = "" Then
    txtid.Text = ""
    txtcodigo.Text = ""
    txtdescripcion.Text = ""
    txtprecioventa.Text = ""
    txtpreciocosto.Text = ""
    txtstock.Text = ""
    cmbrubro.Text = "[Seleccione un rubro]"
End If
Stock_Bajo
End Sub
Sub funcion_Busqueda_grilla()
cn.Open
Set rs = cn.Execute("Select idarticulos, codigo, descripcion, precio_venta, precio_costo, stock, idrubro from articulos where descripcion = '" & FGArticulos.TextMatrix(FGArticulos.Row, 2) & "'")
Do While rs.EOF = False
txtid.Text = rs!idarticulos
txtcodigo.Text = rs!codigo
txtdescripcion.Text = rs!descripcion
txtprecioventa.Text = rs!precio_venta
txtpreciocosto.Text = rs!precio_costo
txtstock.Text = rs!stock
cmbrubro.ListIndex = rs!idrubro - 1
rs.MoveNext
Loop
rs.Close
Set rs = Nothing
cn.Close
Stock_Bajo
End Sub
Sub Rubro()
cn.Open
Set rs = cn.Execute("Listado_Rubro")
Do While rs.EOF = False
cmbrubro.AddItem rs!Rubro
cmbrubro.ItemData(cmbrubro.NewIndex) = rs!idrubro
rs.MoveNext
Loop
rs.Close
Set rs = Nothing
cn.Close
End Sub
Sub funcion_orden()
With cmborden
.AddItem "ID"
.AddItem "Codigo"
.AddItem "Descripci�n"
.AddItem "Rubro"
.AddItem "Stock"
.AddItem "Precio costo"
.AddItem "Precio venta"
End With
End Sub
Sub funcion_Validacion()
variable_contador_validacion = 0
If txtcodigo.Text = "" Then
lblvcodigo.Visible = True
Else
lblvcodigo.Visible = False
variable_contador_validacion = variable_contador_validacion + 1
End If
If txtdescripcion.Text = "" Then
lblvdescripcion.Visible = True
Else
lblvdescripcion.Visible = False
variable_contador_validacion = variable_contador_validacion + 1
End If
If txtprecioventa.Text = "" Then
lblvprecioventa.Visible = True
Else
lblvprecioventa.Visible = False
variable_contador_validacion = variable_contador_validacion + 1
End If
If txtpreciocosto.Text = "" Then
lblvpreciocosto.Visible = True
Else
lblvpreciocosto.Visible = False
variable_contador_validacion = variable_contador_validacion + 1
End If

If cmbrubro.ListIndex = -1 Then
lblvrubro.Visible = True
Else
lblvrubro.Visible = False
variable_contador_validacion = variable_contador_validacion + 1
End If
If txtstock.Text = "" Then
lblvstock.Visible = True
Else
lblvstock.Visible = False
variable_contador_validacion = variable_contador_validacion + 1
End If
End Sub
Sub Stock_Bajo()
For i = 1 To FGArticulos.Rows - 1
If FGArticulos.TextMatrix(i, 3) < 5 Then
If Alerta = "libre" Then
a = MsgBox("Atenci�n! Algunos articulos poseen un stock muy bajo o su stock ha llegado a 0", vbCritical)
Alerta = "ocupado"
End If
FGArticulos.Row = i
FGArticulos.Col = 3
FGArticulos.CellBackColor = &HFF&
End If
Next i
End Sub
