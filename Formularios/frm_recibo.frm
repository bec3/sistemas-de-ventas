VERSION 5.00
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "mshflxgd.ocx"
Begin VB.Form frm_recibo 
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Recibo"
   ClientHeight    =   6870
   ClientLeft      =   945
   ClientTop       =   1455
   ClientWidth     =   9255
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "frm_recibo.frx":0000
   ScaleHeight     =   6870
   ScaleWidth      =   9255
   Begin VB.Frame Frame4 
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      Caption         =   "Frame4"
      Height          =   2895
      Left            =   120
      TabIndex        =   27
      Top             =   3120
      Width           =   9015
      Begin MSHierarchicalFlexGridLib.MSHFlexGrid FGCuentacorriente 
         Height          =   2650
         Left            =   120
         TabIndex        =   28
         Top             =   120
         Width           =   8775
         _ExtentX        =   15478
         _ExtentY        =   4683
         _Version        =   393216
         BackColor       =   16777215
         FixedCols       =   0
         ScrollBars      =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _NumberOfBands  =   1
         _Band(0).Cols   =   2
      End
      Begin VB.Line Line21 
         X1              =   9000
         X2              =   9000
         Y1              =   0
         Y2              =   2880
      End
      Begin VB.Line Line16 
         X1              =   0
         X2              =   9120
         Y1              =   2880
         Y2              =   2880
      End
      Begin VB.Line Line12 
         X1              =   0
         X2              =   0
         Y1              =   0
         Y2              =   2880
      End
      Begin VB.Line Line10 
         X1              =   0
         X2              =   9000
         Y1              =   0
         Y2              =   0
      End
   End
   Begin VB.Frame Frame3 
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      Caption         =   "Frame3"
      Height          =   615
      Left            =   120
      TabIndex        =   24
      Top             =   6120
      Width           =   9015
      Begin VB.CommandButton cmdcancelar 
         Caption         =   "Cancelar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   7440
         TabIndex        =   26
         Top             =   120
         Width           =   1455
      End
      Begin VB.CommandButton cmdaceptar 
         Caption         =   "Aceptar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   5880
         TabIndex        =   25
         Top             =   120
         Width           =   1455
      End
      Begin VB.Line Line20 
         X1              =   9000
         X2              =   9000
         Y1              =   0
         Y2              =   1680
      End
      Begin VB.Line Line17 
         X1              =   0
         X2              =   9000
         Y1              =   600
         Y2              =   600
      End
      Begin VB.Line Line14 
         X1              =   0
         X2              =   0
         Y1              =   0
         Y2              =   1680
      End
      Begin VB.Line Line11 
         X1              =   0
         X2              =   9000
         Y1              =   0
         Y2              =   0
      End
   End
   Begin VB.Frame frmcliente 
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      Caption         =   "Cliente"
      Height          =   1215
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   9015
      Begin VB.CommandButton cmdbuscar 
         Caption         =   "Buscar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   1
         Top             =   120
         Width           =   1095
      End
      Begin VB.Line Line7 
         X1              =   9000
         X2              =   9000
         Y1              =   0
         Y2              =   1680
      End
      Begin VB.Line Line6 
         X1              =   0
         X2              =   0
         Y1              =   0
         Y2              =   1680
      End
      Begin VB.Line Line5 
         X1              =   0
         X2              =   9000
         Y1              =   0
         Y2              =   0
      End
      Begin VB.Line Line3 
         X1              =   0
         X2              =   9000
         Y1              =   1200
         Y2              =   1200
      End
      Begin VB.Label Label13 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Codigo:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1440
         TabIndex        =   11
         Top             =   120
         Width           =   660
      End
      Begin VB.Label lblcodigo 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "codigo"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   2160
         TabIndex        =   10
         Top             =   120
         Width           =   570
      End
      Begin VB.Label lblcondicion 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Condicion IVA"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   6240
         TabIndex        =   9
         Top             =   120
         Width           =   1230
      End
      Begin VB.Label lbldomicilio 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Domicilio"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   2400
         TabIndex        =   8
         Top             =   840
         Width           =   795
      End
      Begin VB.Label lblcuit 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Cuit"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   5400
         TabIndex        =   7
         Top             =   480
         Width           =   345
      End
      Begin VB.Label lblnombre 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "nombre"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   3120
         TabIndex        =   6
         Top             =   480
         Width           =   645
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Apellido y nombre:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1440
         TabIndex        =   5
         Top             =   480
         Width           =   1620
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "CUIT:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   4920
         TabIndex        =   4
         Top             =   480
         Width           =   480
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Domicilio:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1440
         TabIndex        =   3
         Top             =   840
         Width           =   855
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Condici�n IVA:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   4920
         TabIndex        =   2
         Top             =   120
         Width           =   1290
      End
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      Height          =   735
      Left            =   120
      TabIndex        =   19
      Top             =   1440
      Width           =   9015
      Begin VB.Line Line22 
         X1              =   0
         X2              =   9000
         Y1              =   720
         Y2              =   720
      End
      Begin VB.Line Line18 
         X1              =   9000
         X2              =   9000
         Y1              =   0
         Y2              =   1680
      End
      Begin VB.Line Line8 
         X1              =   0
         X2              =   0
         Y1              =   0
         Y2              =   840
      End
      Begin VB.Line Line4 
         X1              =   0
         X2              =   9000
         Y1              =   0
         Y2              =   0
      End
      Begin VB.Label lblsaldo 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Saldo"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   20.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   480
         Left            =   2400
         TabIndex        =   21
         Top             =   120
         Width           =   1035
      End
      Begin VB.Label Label12 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Saldo Total:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   20.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   480
         Left            =   120
         TabIndex        =   20
         Top             =   120
         Width           =   2175
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      Height          =   735
      Left            =   120
      TabIndex        =   12
      Top             =   2280
      Width           =   9015
      Begin VB.TextBox txtmodificado 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         Height          =   285
         Left            =   6120
         TabIndex        =   22
         Top             =   360
         Width           =   2775
      End
      Begin VB.TextBox txtsaldo 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   3120
         TabIndex        =   14
         Top             =   360
         Width           =   2775
      End
      Begin VB.Line Line19 
         X1              =   9000
         X2              =   9000
         Y1              =   0
         Y2              =   1680
      End
      Begin VB.Line Line15 
         X1              =   0
         X2              =   9000
         Y1              =   720
         Y2              =   720
      End
      Begin VB.Line Line13 
         X1              =   0
         X2              =   0
         Y1              =   0
         Y2              =   1680
      End
      Begin VB.Line Line9 
         X1              =   0
         X2              =   9000
         Y1              =   0
         Y2              =   0
      End
      Begin VB.Label Label8 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Nuevo Saldo"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   6840
         TabIndex        =   23
         Top             =   0
         Width           =   1335
      End
      Begin VB.Line Line2 
         X1              =   6000
         X2              =   6000
         Y1              =   0
         Y2              =   720
      End
      Begin VB.Line Line1 
         BorderColor     =   &H80000007&
         X1              =   3000
         X2              =   3000
         Y1              =   0
         Y2              =   840
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Fecha:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   120
         TabIndex        =   18
         Top             =   120
         Width           =   600
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "N� Factura:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   120
         TabIndex        =   17
         Top             =   360
         Width           =   990
      End
      Begin VB.Label lblfecha 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "FECHA"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1320
         TabIndex        =   16
         Top             =   120
         Width           =   660
      End
      Begin VB.Label lblnfactura 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "0000000000000"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1320
         TabIndex        =   15
         Top             =   360
         Width           =   1365
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Ingrese Importe"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   3600
         TabIndex        =   13
         Top             =   0
         Width           =   1605
      End
   End
End
Attribute VB_Name = "frm_recibo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim saldo As Double
Dim modificado As Double
Private Sub cmdbuscar_Click()
Formulario = "recibo"
frm_busqueda_clientes.Show
txtsaldo.Text = ""
txtmodificado.Text = ""
End Sub
Private Sub cmdaceptar_Click()
a = MsgBox("Esta Seguro?", vbYesNo + vbInformation, "Confirmaci�n")
If a = vbYes Then
    If lblcodigo.Caption = "codigo" Then
        MsgBox ("Seleccione un cliente")
    Else
        If txtsaldo.Text = "" Or txtsaldo.Text = "0" Then
            MsgBox ("Ingrese un saldo")
        Else
            cn.Open
            cn.Execute ("insert into cuenta_corriente(fecha,detalle,debe,haber,idcliente) values('" & lblfecha.Caption & "','Recibo " & lblnfactura.Caption & " ', 0, " & Trim(Str(txtsaldo.Text)) & " , " & lblcodigo.Caption & ")")
            cn.Execute ("update parametros set recibo = recibo + 1 where id = 1")
            cn.Close
            Actualizar_Grilla
        End If
    End If
End If
End Sub

Private Sub cmdcancelar_Click()
Unload Me
End Sub

Private Sub Form_Load()
Tama�o_Grilla
lblfecha.Caption = Date
End Sub
Sub Actualizar_Grilla()
cn.Open
    Set rs = cn.Execute("select debe,haber from cuenta_corriente where idcliente = " & lblcodigo.Caption & "")
    saldo = 0
    Do While rs.EOF = False
    saldo = saldo + (rs!debe - rs!haber)
    rs.MoveNext
    Loop
    rs.Close
    Set rs = Nothing
    
    If saldo >= 0 Then
    lblsaldo.ForeColor = &HC000&
    Else
    lblsaldo.ForeColor = &HFF&
    End If
    lblsaldo.Caption = saldo
    Set rs = cn.Execute("select recibo from Parametros where id = 1")
    lblnfactura.Caption = "0001-" & Format(rs!recibo + 1, "00000000")
    rs.Close
    Set rs = Nothing
    Set rs = cn.Execute("Listado_Recibo " & lblcodigo.Caption & "")
    Set FGCuentacorriente.DataSource = rs
    rs.Close
    Set rs = Nothing
cn.Close
    FGCuentacorriente.Rows = 11
    FGCuentacorriente.ColAlignment(2) = flexAlignRightCenter
    FGCuentacorriente.ColAlignment(3) = flexAlignRightCenter
End Sub
Sub Tama�o_Grilla()
With FGCuentacorriente
.Cols = 4
.ColWidth(0) = 1050
.ColWidth(1) = 3600
.ColWidth(2) = 2050
.ColWidth(3) = 2050
End With
End Sub
Private Sub txtsaldo_Change()

    If txtsaldo.Text <> "" And lblsaldo.Caption <> "Saldo" And txtsaldo.Text <> "," Then
    modificado = lblsaldo.Caption
        If txtsaldo.Text < 9999999 Then
            txtmodificado.Text = modificado - Val(Str(txtsaldo.Text))
        Else
            txtsaldo.Text = 9999999
            txtmodificado.Text = modificado - Val(Str(txtsaldo.Text))
        End If
    End If
    If txtsaldo.Text = "" And lblsaldo.Caption <> "Saldo" Then
        txtmodificado.Text = lblsaldo.Caption
    End If
End Sub
Private Sub txtsaldo_KeyPress(KeyAscii As Integer)
strvalid = "0123456789,"
KeyAscii = Asc(Chr(KeyAscii))
If (KeyAscii > 10) Then
If InStr(strvalid, Chr(KeyAscii)) = 0 Then
KeyAscii = 0
End If
End If
End Sub
