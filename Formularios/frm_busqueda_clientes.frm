VERSION 5.00
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "mshflxgd.ocx"
Begin VB.Form frm_busqueda_clientes 
   BackColor       =   &H8000000C&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Busqueda clientes"
   ClientHeight    =   10215
   ClientLeft      =   585
   ClientTop       =   915
   ClientWidth     =   12390
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "frm_busqueda_clientes.frx":0000
   ScaleHeight     =   10215
   ScaleWidth      =   12390
   Begin VB.Frame Frame2 
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      Caption         =   "Frame2"
      Height          =   9255
      Left            =   120
      TabIndex        =   1
      Top             =   840
      Width           =   12135
      Begin MSHierarchicalFlexGridLib.MSHFlexGrid FGClientes 
         Height          =   9015
         Left            =   120
         TabIndex        =   2
         Top             =   120
         Width           =   11895
         _ExtentX        =   20981
         _ExtentY        =   15901
         _Version        =   393216
         FixedCols       =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _NumberOfBands  =   1
         _Band(0).Cols   =   2
      End
      Begin VB.Line Line4 
         X1              =   0
         X2              =   12120
         Y1              =   0
         Y2              =   0
      End
      Begin VB.Line Line3 
         X1              =   12120
         X2              =   12120
         Y1              =   0
         Y2              =   9240
      End
      Begin VB.Line Line2 
         X1              =   12120
         X2              =   0
         Y1              =   9240
         Y2              =   9240
      End
      Begin VB.Line Line1 
         X1              =   0
         X2              =   0
         Y1              =   0
         Y2              =   9480
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   615
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   12135
      Begin VB.TextBox txtbuscar 
         Appearance      =   0  'Flat
         Height          =   375
         Left            =   120
         TabIndex        =   3
         Top             =   120
         Width           =   2655
      End
      Begin VB.Line Line8 
         X1              =   12120
         X2              =   12120
         Y1              =   0
         Y2              =   720
      End
      Begin VB.Line Line7 
         X1              =   0
         X2              =   0
         Y1              =   0
         Y2              =   600
      End
      Begin VB.Line Line6 
         X1              =   0
         X2              =   12120
         Y1              =   600
         Y2              =   600
      End
      Begin VB.Line Line5 
         X1              =   0
         X2              =   12120
         Y1              =   0
         Y2              =   0
      End
   End
End
Attribute VB_Name = "frm_busqueda_clientes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim saldo As Double
Private Sub FGClientes_DblClick()
If Formulario = "recibo" Then
    frm_recibo.lblcodigo.Caption = FGClientes.TextMatrix(FGClientes.Row, 0)
    frm_recibo.lblnombre.Caption = FGClientes.TextMatrix(FGClientes.Row, 1)
    frm_recibo.lbldomicilio.Caption = FGClientes.TextMatrix(FGClientes.Row, 2)
    frm_recibo.lblcuit.Caption = FGClientes.TextMatrix(FGClientes.Row, 4)
    frm_recibo.lblcondicion.Caption = FGClientes.TextMatrix(FGClientes.Row, 6)
    frm_recibo.Actualizar_Grilla
    frm_recibo.Show
End If
If Formulario = "facturacion" Then 'Formulario Facturacion
    frm_facturacion.Estado_Inicial
    frm_facturacion.lblcodigo.Caption = FGClientes.TextMatrix(FGClientes.Row, 0)
    frm_facturacion.lblnombre.Caption = FGClientes.TextMatrix(FGClientes.Row, 1)
    frm_facturacion.lbldomicilio.Caption = FGClientes.TextMatrix(FGClientes.Row, 2)
    frm_facturacion.lblcuit.Caption = FGClientes.TextMatrix(FGClientes.Row, 4)
    frm_facturacion.lblcondicion.Caption = FGClientes.TextMatrix(FGClientes.Row, 6)
    cn.Open
    Set rs = cn.Execute("select debe,haber from cuenta_corriente where idcliente = " & FGClientes.TextMatrix(FGClientes.Row, 0) & "")
    saldo = 0
    Do While rs.EOF = False
        saldo = saldo + (rs!debe - rs!haber)
        rs.MoveNext
    Loop
    rs.Close
    Set rs = Nothing
    cn.Close
    If saldo >= 0 Then
    frm_facturacion.lblsaldo.ForeColor = &HC000&
    Else
    frm_facturacion.lblsaldo.ForeColor = &HFF&
    End If
    frm_facturacion.lblsaldo.Caption = saldo
    If frm_facturacion.lblcondicion.Caption = "Responsable Inscripto" Then
    frm_facturacion.lbltipofactura.Caption = "A"
    Else
    frm_facturacion.lbltipofactura.Caption = "B"
    End If
    frm_facturacion.Show
    cn.Open
    Set rs = cn.Execute("select id,facturas_a,facturas_b from Parametros")
    If frm_facturacion.lbltipofactura.Caption = "A" Then
        frm_facturacion.lblnfactura.Caption = "0001-" & Format(rs!facturas_a + 1, "00000000")
    Else
        frm_facturacion.lblnfactura.Caption = "0001-" & Format(rs!facturas_b + 1, "00000000")
    End If
    rs.Close
    Set rs = Nothing
    cn.Close
    frm_facturacion.frmencabezado.Enabled = True
    frm_facturacion.Borderusuario.BorderColor = &H0&
    frm_facturacion.color_encabezado
    frm_facturacion.cmbcomprobante.SetFocus
End If
If Formulario = "cuentacorriente" Then
    frm_cuenta_corriente.DTDesde.Value = Date - 7
    frm_cuenta_corriente.DTHasta.Value = Date
    frm_cuenta_corriente.lblcodigo.Caption = FGClientes.TextMatrix(FGClientes.Row, 0)
    frm_cuenta_corriente.lblnombre.Caption = FGClientes.TextMatrix(FGClientes.Row, 1)
    frm_cuenta_corriente.lbldomicilio.Caption = FGClientes.TextMatrix(FGClientes.Row, 2)
    frm_cuenta_corriente.lblcuit.Caption = FGClientes.TextMatrix(FGClientes.Row, 4)
    frm_cuenta_corriente.lblcondicion.Caption = FGClientes.TextMatrix(FGClientes.Row, 6)
    cn.Open
    Set rs = cn.Execute("select debe,haber from cuenta_corriente where idcliente = " & FGClientes.TextMatrix(FGClientes.Row, 0) & "")
    frm_cuenta_corriente.saldo = 0
    Do While rs.EOF = False
        frm_cuenta_corriente.saldo = frm_cuenta_corriente.saldo + (rs!debe - rs!haber)
        rs.MoveNext
    Loop
    rs.Close
    Set rs = Nothing
    cn.Close
    If frm_cuenta_corriente.saldo >= 0 Then
        frm_cuenta_corriente.lblsaldo.ForeColor = &HC000&
    Else
        frm_cuenta_corriente.lblsaldo.ForeColor = &HFF&
    End If
    frm_cuenta_corriente.lblsaldo.Caption = frm_cuenta_corriente.saldo
    frm_cuenta_corriente.obsemana.Value = True
    frm_cuenta_corriente.ActualizarLista
    frm_cuenta_corriente.FGCuentacorriente.Enabled = True
End If
Unload Me
End Sub
Private Sub txtbuscar_Change()
funcion_Busqueda_cuadro
End Sub
Private Sub Form_Load()
ActualizarLista
Tama�o_Grilla
End Sub
Sub Tama�o_Grilla()
With FGClientes
.Cols = 8
.ColWidth(0) = 700
.ColWidth(1) = 2000
.ColWidth(2) = 2000
.ColWidth(3) = 1000
.ColWidth(4) = 1000
.ColWidth(5) = 1000
.ColWidth(6) = 2000
.ColWidth(7) = 2000
.ColWidth(0) = 700
.ColWidth(1) = 1700
.ColWidth(2) = 1700
.ColWidth(3) = 1150
.ColWidth(4) = 1150
.ColWidth(5) = 1300
.ColWidth(6) = 1750
.ColWidth(7) = 2055
.TextArray(0) = "Codigo"
.TextArray(1) = "Apellido y Nombre"
.TextArray(2) = "Domicilio"
.TextArray(3) = "Telefono"
.TextArray(4) = "Cuit/DNI"
.TextArray(5) = "Localidad"
.TextArray(6) = "Categor�a"
.TextArray(7) = "Correo"
End With
End Sub
Sub ActualizarLista()
cn.Open
Set rs = cn.Execute("Listado_Clientes")
Set FGClientes.DataSource = rs
rs.Close
Set rs = Nothing
cn.Close
End Sub
Sub funcion_Busqueda_cuadro()
FGClientes.Rows = 1
For i = 0 To 7
FGClientes.Col = i
FGClientes.CellBackColor = &H8000000F
Next i
cn.Open
Set rs = cn.Execute("Select  Cliente.idcliente, Cliente.nombre, Cliente.domicilio, Cliente.telefono, Cliente.cuit, Cliente.idlocalidad, Cliente.idcategoria, Cliente.email, Categoria.categoria, Localidad.localidad FROM Cliente INNER JOIN Categoria ON Cliente.idcategoria = Categoria.idcategoria INNER JOIN localidad ON Cliente.idlocalidad = localidad.idlocalidad where nombre like" & " '" & "" & txtbuscar.Text & "%' ORDER By cliente.nombre ASC")
Do While rs.EOF = False
With FGClientes
.Rows = .Rows + 1
.TextMatrix(.Rows - 1, 0) = rs!idcliente
.TextMatrix(.Rows - 1, 1) = rs!nombre
.TextMatrix(.Rows - 1, 2) = rs!domicilio
.TextMatrix(.Rows - 1, 3) = rs!telefono
.TextMatrix(.Rows - 1, 4) = rs!cuit
.TextMatrix(.Rows - 1, 5) = rs!Localidad
.TextMatrix(.Rows - 1, 6) = rs!categoria
.TextMatrix(.Rows - 1, 7) = rs!email
End With
rs.MoveNext
Loop
rs.Close
Set rs = Nothing
cn.Close
End Sub
