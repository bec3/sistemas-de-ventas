VERSION 5.00
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "mshflxgd.ocx"
Begin VB.Form frm_facturacion 
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Facturaci�n"
   ClientHeight    =   10950
   ClientLeft      =   225
   ClientTop       =   555
   ClientWidth     =   20025
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "frm_facturacion.frx":0000
   ScaleHeight     =   10950
   ScaleWidth      =   20025
   Begin VB.Frame frmdetalle 
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      ForeColor       =   &H8000000D&
      Height          =   5895
      Left            =   120
      TabIndex        =   2
      Top             =   3720
      Width           =   9975
      Begin VB.CommandButton cmdbuscararticulos 
         Caption         =   "Articulos"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   8640
         TabIndex        =   37
         Top             =   5400
         Width           =   1215
      End
      Begin MSHierarchicalFlexGridLib.MSHFlexGrid FGDetalle 
         Height          =   5175
         Left            =   120
         TabIndex        =   5
         Top             =   120
         Width           =   9735
         _ExtentX        =   17171
         _ExtentY        =   9128
         _Version        =   393216
         ScrollBars      =   2
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _NumberOfBands  =   1
         _Band(0).Cols   =   2
         _Band(0).GridLinesBand=   1
         _Band(0).TextStyleBand=   0
         _Band(0).TextStyleHeader=   0
      End
      Begin VB.Shape borderdetalle 
         BorderColor     =   &H00000000&
         Height          =   5895
         Left            =   0
         Top             =   0
         Width           =   9975
      End
   End
   Begin VB.Frame frmtotales 
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      Height          =   1095
      Left            =   120
      TabIndex        =   3
      Top             =   9720
      Width           =   9975
      Begin VB.TextBox txttotal 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   5880
         TabIndex        =   27
         Top             =   360
         Width           =   2295
      End
      Begin VB.TextBox txtiva 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   3480
         TabIndex        =   26
         Top             =   360
         Width           =   1455
      End
      Begin VB.TextBox txtneto 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   720
         TabIndex        =   22
         Top             =   360
         Width           =   2055
      End
      Begin VB.CommandButton cmdcancelar 
         Caption         =   "Cancelar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   8400
         TabIndex        =   20
         Top             =   600
         Width           =   1455
      End
      Begin VB.CommandButton cmdaceptar 
         Caption         =   "Aceptar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   8400
         TabIndex        =   19
         Top             =   120
         Width           =   1455
      End
      Begin VB.Line Line27 
         X1              =   9960
         X2              =   9960
         Y1              =   0
         Y2              =   2160
      End
      Begin VB.Line Line26 
         X1              =   0
         X2              =   10080
         Y1              =   1080
         Y2              =   1080
      End
      Begin VB.Line Line17 
         X1              =   0
         X2              =   0
         Y1              =   0
         Y2              =   2160
      End
      Begin VB.Line Line9 
         Index           =   1
         X1              =   0
         X2              =   9960
         Y1              =   0
         Y2              =   0
      End
      Begin VB.Line Line6 
         BorderColor     =   &H80000007&
         X1              =   8280
         X2              =   8280
         Y1              =   0
         Y2              =   1200
      End
      Begin VB.Label Label10 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Neto"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   240
         TabIndex        =   25
         Top             =   360
         Width           =   405
      End
      Begin VB.Label Label9 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Iva"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   3120
         TabIndex        =   24
         Top             =   360
         Width           =   225
      End
      Begin VB.Label Label8 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Total"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   5160
         TabIndex        =   23
         Top             =   360
         Width           =   420
      End
   End
   Begin VB.Frame frmfiltro 
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      Height          =   1575
      Left            =   10200
      TabIndex        =   38
      Top             =   120
      Width           =   9735
      Begin VB.Frame Frame1 
         Appearance      =   0  'Flat
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         Caption         =   " Buscar "
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   1095
         Left            =   7920
         TabIndex        =   40
         Top             =   240
         Width           =   1695
         Begin VB.OptionButton OBcodigo 
            BackColor       =   &H00C0C0C0&
            Caption         =   "Codigo"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   375
            Left            =   120
            TabIndex        =   42
            Top             =   120
            Value           =   -1  'True
            Width           =   1455
         End
         Begin VB.OptionButton OBdescripcion 
            BackColor       =   &H00C0C0C0&
            Caption         =   "Descripci�n"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   120
            TabIndex        =   41
            Top             =   720
            Width           =   1455
         End
         Begin VB.Line Line42 
            X1              =   0
            X2              =   1680
            Y1              =   0
            Y2              =   0
         End
         Begin VB.Line Line41 
            X1              =   1680
            X2              =   1680
            Y1              =   0
            Y2              =   1200
         End
         Begin VB.Line Line40 
            X1              =   0
            X2              =   1800
            Y1              =   1080
            Y2              =   1080
         End
         Begin VB.Line Line39 
            X1              =   0
            X2              =   0
            Y1              =   0
            Y2              =   1200
         End
      End
      Begin VB.TextBox txtbuscar 
         Appearance      =   0  'Flat
         Height          =   405
         Left            =   120
         ScrollBars      =   2  'Vertical
         TabIndex        =   39
         Top             =   240
         Width           =   2535
      End
      Begin VB.Frame Frame2 
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         Caption         =   "Filtro"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   615
         Left            =   6120
         TabIndex        =   43
         Top             =   240
         Width           =   1695
         Begin VB.ComboBox cmbfiltro 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   120
            TabIndex        =   44
            Text            =   "[Seleccione]"
            Top             =   120
            Width           =   1455
         End
         Begin VB.Line Line46 
            X1              =   1680
            X2              =   1680
            Y1              =   0
            Y2              =   600
         End
         Begin VB.Line Line45 
            X1              =   0
            X2              =   1800
            Y1              =   600
            Y2              =   600
         End
         Begin VB.Line Line44 
            X1              =   0
            X2              =   0
            Y1              =   0
            Y2              =   600
         End
         Begin VB.Line Line43 
            X1              =   0
            X2              =   1680
            Y1              =   0
            Y2              =   0
         End
      End
      Begin VB.Line Line13 
         X1              =   0
         X2              =   9960
         Y1              =   0
         Y2              =   0
      End
      Begin VB.Line Line38 
         X1              =   9720
         X2              =   9720
         Y1              =   0
         Y2              =   1560
      End
      Begin VB.Line Line37 
         X1              =   0
         X2              =   10080
         Y1              =   1560
         Y2              =   1560
      End
      Begin VB.Line Line18 
         X1              =   0
         X2              =   0
         Y1              =   0
         Y2              =   2160
      End
      Begin VB.Label lblingrese 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Ingrese Codigo del articulo"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   2760
         TabIndex        =   45
         Top             =   240
         Width           =   2295
      End
   End
   Begin VB.Frame frmarticulos 
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      Height          =   8295
      Left            =   10200
      TabIndex        =   30
      Top             =   1800
      Width           =   9735
      Begin MSHierarchicalFlexGridLib.MSHFlexGrid FGArticulos 
         Height          =   8055
         Left            =   120
         TabIndex        =   31
         Top             =   120
         Width           =   9495
         _ExtentX        =   16748
         _ExtentY        =   14208
         _Version        =   393216
         Rows            =   3
         FixedCols       =   0
         Enabled         =   0   'False
         ScrollBars      =   2
         Appearance      =   0
         _NumberOfBands  =   1
         _Band(0).Cols   =   2
      End
      Begin VB.Line Line34 
         X1              =   9720
         X2              =   9720
         Y1              =   0
         Y2              =   8280
      End
      Begin VB.Line Line33 
         X1              =   9720
         X2              =   0
         Y1              =   8280
         Y2              =   8280
      End
      Begin VB.Line Line19 
         X1              =   0
         X2              =   0
         Y1              =   0
         Y2              =   8280
      End
      Begin VB.Line Line12 
         X1              =   0
         X2              =   9960
         Y1              =   0
         Y2              =   0
      End
   End
   Begin VB.Frame frmencabezado 
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      Height          =   2175
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   9975
      Begin VB.ComboBox cmbcomprobante 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   7920
         TabIndex        =   46
         Text            =   "[Seleccione]"
         Top             =   840
         Width           =   1815
      End
      Begin VB.Frame frmpago 
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         Caption         =   "Tipo de pago"
         Height          =   615
         Left            =   6360
         TabIndex        =   6
         Top             =   1320
         Width           =   3375
         Begin VB.OptionButton OBctacorriente 
            BackColor       =   &H00C0C0C0&
            Caption         =   "Cta.Corriente"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   270
            Left            =   1440
            TabIndex        =   8
            Top             =   240
            Width           =   1815
         End
         Begin VB.OptionButton OBContado 
            BackColor       =   &H00C0C0C0&
            Caption         =   "Contado"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   270
            Left            =   120
            TabIndex        =   7
            Top             =   225
            Value           =   -1  'True
            Width           =   1335
         End
         Begin VB.Line Line32 
            X1              =   3360
            X2              =   3360
            Y1              =   120
            Y2              =   600
         End
         Begin VB.Line Line30 
            X1              =   0
            X2              =   3360
            Y1              =   120
            Y2              =   120
         End
         Begin VB.Line Line29 
            X1              =   0
            X2              =   0
            Y1              =   120
            Y2              =   600
         End
         Begin VB.Line Line28 
            X1              =   0
            X2              =   3360
            Y1              =   600
            Y2              =   600
         End
      End
      Begin VB.Line borderencabezado3 
         X1              =   5760
         X2              =   9960
         Y1              =   0
         Y2              =   0
      End
      Begin VB.Line borderencabezado5 
         X1              =   5760
         X2              =   4800
         Y1              =   1560
         Y2              =   1560
      End
      Begin VB.Line Line10 
         X1              =   0
         X2              =   5760
         Y1              =   0
         Y2              =   0
      End
      Begin VB.Line Line5 
         X1              =   0
         X2              =   4800
         Y1              =   2160
         Y2              =   2160
      End
      Begin VB.Line Line31 
         X1              =   9720
         X2              =   9720
         Y1              =   1440
         Y2              =   1920
      End
      Begin VB.Line borderencabezado2 
         X1              =   9960
         X2              =   9960
         Y1              =   0
         Y2              =   2160
      End
      Begin VB.Line borderencabezado1 
         BorderColor     =   &H00000000&
         X1              =   4800
         X2              =   10080
         Y1              =   2160
         Y2              =   2160
      End
      Begin VB.Line Line7 
         X1              =   0
         X2              =   0
         Y1              =   0
         Y2              =   2160
      End
      Begin VB.Image Image1 
         Height          =   1710
         Left            =   120
         Picture         =   "frm_facturacion.frx":6FD2
         Top             =   120
         Width           =   1695
      End
      Begin VB.Label Label15 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "CUIL/CUIT:20-36922649-6"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   1920
         TabIndex        =   54
         Top             =   1680
         Width           =   2805
      End
      Begin VB.Label Label14 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Tel: 2364702163"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   1920
         TabIndex        =   53
         Top             =   1320
         Width           =   1785
      End
      Begin VB.Label Label11 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "BC Corporation"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   1920
         TabIndex        =   52
         Top             =   960
         Width           =   1650
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Fecha:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   6240
         TabIndex        =   51
         Top             =   120
         Width           =   720
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Comprobante:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   6240
         TabIndex        =   50
         Top             =   840
         Width           =   1500
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "N� Factura:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   6240
         TabIndex        =   49
         Top             =   480
         Width           =   1170
      End
      Begin VB.Label lblfecha 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "FECHA"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   7920
         TabIndex        =   48
         Top             =   120
         Width           =   825
      End
      Begin VB.Label lblnfactura 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "0000000000000"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   7920
         TabIndex        =   47
         Top             =   480
         Width           =   1755
      End
      Begin VB.Label lbltipofactura 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "A"
         BeginProperty Font 
            Name            =   "Arial Narrow"
            Size            =   80.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1815
         Left            =   4335
         TabIndex        =   4
         Top             =   -120
         Width           =   975
      End
      Begin VB.Line Line4 
         BorderColor     =   &H80000007&
         X1              =   4800
         X2              =   3840
         Y1              =   1560
         Y2              =   1560
      End
      Begin VB.Line borderencabezado4 
         BorderColor     =   &H80000007&
         X1              =   5760
         X2              =   5760
         Y1              =   0
         Y2              =   1560
      End
      Begin VB.Line Line2 
         BorderColor     =   &H80000007&
         X1              =   3840
         X2              =   3840
         Y1              =   0
         Y2              =   1570
      End
      Begin VB.Line borderencabezado6 
         BorderColor     =   &H80000007&
         X1              =   4800
         X2              =   4800
         Y1              =   1560
         Y2              =   2160
      End
   End
   Begin VB.Frame Frame3 
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      Height          =   615
      Left            =   10200
      TabIndex        =   32
      Top             =   10200
      Width           =   9735
      Begin VB.CommandButton cmdcancelar_articulos 
         BackColor       =   &H00000000&
         Caption         =   "Cancelar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   7920
         TabIndex        =   36
         Top             =   120
         Width           =   1695
      End
      Begin VB.CommandButton cmdeliminar 
         Caption         =   "Eliminar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3240
         TabIndex        =   35
         Top             =   120
         Width           =   1455
      End
      Begin VB.CommandButton cmdmodificar 
         Caption         =   "Modificar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1680
         TabIndex        =   34
         Top             =   120
         Width           =   1455
      End
      Begin VB.CommandButton cmdagregar 
         Caption         =   "Agregar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   33
         Top             =   120
         Width           =   1455
      End
      Begin VB.Line Line36 
         X1              =   9720
         X2              =   9720
         Y1              =   0
         Y2              =   600
      End
      Begin VB.Line Line35 
         X1              =   0
         X2              =   9840
         Y1              =   600
         Y2              =   600
      End
      Begin VB.Line Line20 
         X1              =   0
         X2              =   0
         Y1              =   0
         Y2              =   2160
      End
      Begin VB.Line Line14 
         X1              =   0
         X2              =   9960
         Y1              =   0
         Y2              =   0
      End
   End
   Begin VB.Frame frmcliente 
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      Height          =   1215
      Left            =   120
      TabIndex        =   1
      Top             =   2400
      Width           =   9975
      Begin VB.CommandButton cmdnuevo 
         Caption         =   "Nuevo"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   55
         Top             =   720
         Width           =   1095
      End
      Begin VB.CommandButton cmdbuscar 
         Caption         =   "Buscar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   21
         Top             =   120
         Width           =   1095
      End
      Begin VB.Shape Borderusuario 
         BorderColor     =   &H00000000&
         Height          =   1215
         Left            =   0
         Top             =   0
         Width           =   1335
      End
      Begin VB.Shape Shape2 
         Height          =   1215
         Left            =   1320
         Top             =   0
         Width           =   8655
      End
      Begin VB.Label Label13 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Codigo:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1560
         TabIndex        =   29
         Top             =   120
         Width           =   660
      End
      Begin VB.Label lblcodigo 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "codigo"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   2280
         TabIndex        =   28
         Top             =   120
         Width           =   570
      End
      Begin VB.Label lblsaldo 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Saldo"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Left            =   6000
         TabIndex        =   18
         Top             =   840
         Width           =   495
      End
      Begin VB.Label Label12 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Saldo:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   5400
         TabIndex        =   17
         Top             =   840
         Width           =   555
      End
      Begin VB.Label lblcondicion 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Condicion IVA"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   6840
         TabIndex        =   16
         Top             =   480
         Width           =   1230
      End
      Begin VB.Label lbldomicilio 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Domicilio"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   2520
         TabIndex        =   15
         Top             =   840
         Width           =   795
      End
      Begin VB.Label lblcuit 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Cuit"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   6000
         TabIndex        =   14
         Top             =   120
         Width           =   345
      End
      Begin VB.Label lblnombre 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "nombre"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   3360
         TabIndex        =   13
         Top             =   480
         Width           =   645
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Apellido y nombre:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1560
         TabIndex        =   12
         Top             =   480
         Width           =   1620
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "CUIT:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   5400
         TabIndex        =   11
         Top             =   120
         Width           =   480
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Domicilio:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1560
         TabIndex        =   10
         Top             =   840
         Width           =   855
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Condici�n IVA:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   5400
         TabIndex        =   9
         Top             =   480
         Width           =   1290
      End
   End
End
Attribute VB_Name = "frm_facturacion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public neto As Double
Public iva As Double
Public total As Double
Public EstadoDetalle As String
Dim Buscar_articulo As String
Private Sub cmbcomprobante_Click()
If cmbcomprobante.Text = "Nota de Credito" Or cmbcomprobante.Text = "Nota de Debito" Then
OBctacorriente.Value = True
OBContado.Enabled = False
Else
OBContado.Enabled = True
OBContado.Value = True
End If
FGDetalle.Rows = 1
frm_facturacion.Width = 10275
ActualizarGrilla
validacion_encabezado
txtneto.Text = ""
txtiva.Text = ""
txttotal.Text = ""
End Sub
Private Sub cmbcomprobante_KeyPress(KeyAscii As Integer)
KeyAscii = combos_letras(KeyAscii)
End Sub

Private Sub cmbfiltro_KeyPress(KeyAscii As Integer)
KeyAscii = combos_letras(KeyAscii)
End Sub
Private Sub cmdaceptar_Click()
a = MsgBox("Esta Seguro?", vbInformation + vbYesNo, "Confirmaci�n")
If a = vbYes Then
If FGDetalle.Rows > 1 Then
cn.Open
If lbltipofactura.Caption = "A" Then
    cn.Execute ("update parametros set facturas_a = facturas_a + 1 where id = 1")
Else
    cn.Execute ("update parametros set facturas_b = facturas_b + 1 where id = 1")
End If
If OBContado.Value = True Then
    variable_condiciondeventa = 1
Else
    variable_condiciondeventa = 2
End If
'registro de venta
cn.Execute ("insert into Ventas(fecha,letra_c,numero,neto,iva,total,idtipo_venta,idcondicion_venta,idcliente) values ('" & lblfecha.Caption & "','" & lbltipofactura.Caption & "','" & lblnfactura.Caption & "'," & Trim(Str(neto)) & "," & Trim(Str(iva)) & " ," & Trim(Str(total)) & "," & cmbcomprobante.ItemData(cmbcomprobante.ListIndex) & "," & variable_condiciondeventa & ", " & lblcodigo.Caption & " )")
Set rs = cn.Execute("select max(idventas) as id from ventas")

'registro detalle de venta
variable_idventa = rs!id
For i = 1 To FGDetalle.Rows - 1
Set rs = cn.Execute("select idarticulos,precio_costo,precio_venta from articulos where idarticulos = " & FGDetalle.TextMatrix(i, 0) & "")
cn.Execute ("insert into detalle_venta(cantidad,precio_venta,precio_costo,idarticulos,idventas) values (" & FGDetalle.TextMatrix(i, 4) & "," & Trim(Str(rs!precio_venta)) & ", " & Trim(Str(rs!precio_costo)) & ", " & rs!idarticulos & ", " & variable_idventa & ")")
Next i

'actualizar stock
If cmbcomprobante.Text = "Nota de Credito" Then
For i = 1 To FGDetalle.Rows - 1
cn.Execute ("update articulos set stock = stock + " & FGDetalle.TextMatrix(i, 4) & " where idarticulos = " & FGDetalle.TextMatrix(i, 0) & "")
Next i
Else
For i = 1 To FGDetalle.Rows - 1
cn.Execute ("update articulos set stock = stock - " & FGDetalle.TextMatrix(i, 4) & " where idarticulos = " & FGDetalle.TextMatrix(i, 0) & "")
Next i
End If

'cuenta corriente
If OBctacorriente.Value = True Then
If cmbcomprobante.Text = "Nota de Credito" Then
cn.Execute ("insert into cuenta_corriente(fecha,detalle,debe,haber,idcliente) values('" & lblfecha.Caption & "','" & cmbcomprobante.Text & " " & lbltipofactura.Caption & " " & lblnfactura.Caption & " ', 0, " & Trim(Str(total)) & " , " & lblcodigo.Caption & ")")
Else
cn.Execute ("insert into cuenta_corriente(fecha,detalle,debe,haber,idcliente) values('" & lblfecha.Caption & "','" & cmbcomprobante.Text & " " & lbltipofactura.Caption & " " & lblnfactura.Caption & " ', " & Trim(Str(total)) & ", " & 0 & " , " & lblcodigo.Caption & ")")
End If
End If
rs.Close
cn.Close
frmmenuprincipal.Actualizar_Lista
Estado_Inicial
Else
MsgBox ("Error, debe completar todos los campos")
End If
End If
End Sub
Private Sub cmdbuscar_Click()
Formulario = "facturacion"
frm_busqueda_clientes.Show
End Sub
Private Sub cmdcancelar_Click()
Unload Me
End Sub

Private Sub cmdnuevo_Click()
mnabmclientes.Show
mnabmclientes.cmdnuevo_Click
Formulario = "facturacion"
End Sub
Private Sub Form_Load()
Declarar_Grilla
Comprobante
filtro_articulos
lblfecha.Caption = Date 'declarar fecha
Estado_Inicial
End Sub
Sub Declarar_Grilla()
FGDetalle.Rows = 1
With FGDetalle
.Cols = 6
.ColWidth(0) = 0
.ColWidth(1) = 0
.ColWidth(2) = 3900
.ColWidth(3) = 2000
.ColWidth(4) = 1100
.ColWidth(5) = 2735
.TextArray(0) = "Id Articulo"
.TextArray(1) = "Codigo"
.TextArray(2) = "Descripci�n"
.TextArray(3) = "Precio unitario"
.TextArray(4) = "Cantidad"
.TextArray(5) = "Precio total"
.ColAlignment(3) = flexAlignRightCenter
.ColAlignment(5) = flexAlignRightCenter
End With
End Sub
Sub Comprobante()
cn.Open
Set rs = cn.Execute("Listado_Tipo_venta")
Do While rs.EOF = False
cmbcomprobante.AddItem rs!tipo
cmbcomprobante.ItemData(cmbcomprobante.NewIndex) = rs!idtipo_venta
rs.MoveNext
Loop
rs.Close
Set rs = Nothing
cn.Close
End Sub
Sub validacion_encabezado()
If cmbcomprobante.ListIndex <> -1 Then
frmdetalle.Enabled = True
borderdetalle.BorderColor = &HFF&
color_encabezado
End If
End Sub

Private Sub OBContado_Click()
validacion_encabezado
End Sub
Private Sub OBctacorriente_Click()
validacion_encabezado
End Sub
Private Sub cmdbuscararticulos_Click()
If frm_facturacion.Width = 10275 Then
frm_facturacion.Width = 20115
cmdagregar_Click
Else
frm_facturacion.Width = 10275
End If
End Sub
' cambio
Private Sub cmbfiltro_click()
Tama�o_Grilla_articulos
cn.Open
Set rs = cn.Execute("SELECT Articulos.idarticulos, Articulos.codigo, Articulos.descripcion, Articulos.precio_venta, Articulos.precio_costo, Articulos.stock, Rubro.rubro FROM Articulos INNER JOIN Rubro ON Articulos.idrubro = Rubro.idrubro where articulos.idrubro = " & cmbfiltro.ListIndex + 1 & "  ORDER By descripcion ASC")
With FGArticulos
Do While rs.EOF = False
.Rows = .Rows + 1
.TextMatrix(.Rows - 1, 0) = rs!idarticulos
.TextMatrix(.Rows - 1, 1) = rs!codigo
.TextMatrix(.Rows - 1, 2) = rs!descripcion
.TextMatrix(.Rows - 1, 3) = rs!Rubro
.TextMatrix(.Rows - 1, 4) = rs!stock
.TextMatrix(.Rows - 1, 5) = rs!precio_costo
.TextMatrix(.Rows - 1, 6) = rs!precio_venta
rs.MoveNext
Loop
End With
rs.Close
Set rs = Nothing
cn.Close
End Sub
Private Sub cmdagregar_Click()
EstadoDetalle = "agregar"
cmdagregar.Enabled = False
cmdmodificar.Enabled = True
cmdeliminar.Enabled = True
FGArticulos.Enabled = True
End Sub
Private Sub cmdeliminar_Click()
cmdagregar.Enabled = True
cmdmodificar.Enabled = True
cmdeliminar.Enabled = False
FGArticulos.Enabled = False
FGDetalle.Enabled = True
EstadoDetalle = "eliminar"
End Sub
Private Sub cmdmodificar_Click()
cmdagregar.Enabled = True
cmdmodificar.Enabled = False
cmdeliminar.Enabled = True
FGArticulos.Enabled = False
FGDetalle.Enabled = True
EstadoDetalle = "modificar"
End Sub
Private Sub cmdlimpiar_Click()
seguro = MsgBox("Esta Seguro?", vbYesNo + vbInformation, "Seguridad")
If seguro = vbYes Then
txtbuscar.Text = ""
cmbfiltro.Text = "[Seleccione]"
Form_Load
End If
End Sub
Private Sub FGArticulos_DblClick()
If FGArticulos.Row <> 0 Then
frm_busqueda_articulos_cantidad.posicion_modificar = FGArticulos.Row
frm_busqueda_articulos_cantidad.Show
frm_busqueda_articulos_cantidad.txtdescripcion.Text = FGArticulos.TextMatrix(FGArticulos.Row, 2)
frm_busqueda_articulos_cantidad.txtunitario.Text = FGArticulos.TextMatrix(FGArticulos.Row, 6)
frm_busqueda_articulos_cantidad.txtcantidad.SetFocus
End If
End Sub
Private Sub FGDetalle_dblClick()
If FGDetalle.Row <> 0 And EstadoDetalle = "eliminar" Then
If cmbcomprobante.Text <> "Nota de Credito" Then
    For i = 1 To FGArticulos.Rows - 1
        If FGArticulos.TextMatrix(i, 2) = FGDetalle.TextMatrix(FGDetalle.Row, 2) Then
            FGArticulos.TextMatrix(i, 4) = Val(FGArticulos.TextMatrix(i, 4)) + Val(FGDetalle.TextMatrix(FGDetalle.Row, 4))
        End If
    Next i
End If

For i = FGDetalle.Row To FGDetalle.Rows - 2
FGDetalle.TextMatrix(i, 0) = FGDetalle.TextMatrix(i + 1, 0)
FGDetalle.TextMatrix(i, 1) = FGDetalle.TextMatrix(i + 1, 1)
FGDetalle.TextMatrix(i, 2) = FGDetalle.TextMatrix(i + 1, 2)
FGDetalle.TextMatrix(i, 3) = FGDetalle.TextMatrix(i + 1, 3)
FGDetalle.TextMatrix(i, 4) = FGDetalle.TextMatrix(i + 1, 4)
FGDetalle.TextMatrix(i, 5) = FGDetalle.TextMatrix(i + 1, 5)
Next i
FGDetalle.Rows = FGDetalle.Rows - 1

End If
If FGDetalle.Row <> 0 And EstadoDetalle = "modificar" Then
    If FGDetalle.Row <> 0 Then
        frm_busqueda_articulos_cantidad.posicion_modificar = FGDetalle.Row
        frm_busqueda_articulos_cantidad.Show
        frm_busqueda_articulos_cantidad.txtdescripcion.Text = FGDetalle.TextMatrix(FGDetalle.Row, 2)
        frm_busqueda_articulos_cantidad.txtcantidad.Text = FGDetalle.TextMatrix(FGDetalle.Row, 4)
        frm_busqueda_articulos_cantidad.txttotal.Text = FGDetalle.TextMatrix(FGDetalle.Row, 5)
        frm_busqueda_articulos_cantidad.txtunitario.Text = FGDetalle.TextMatrix(FGDetalle.Row, 3)
        frm_busqueda_articulos_cantidad.txtcantidad.SetFocus
    End If
End If
calcular_totales
End Sub
Private Sub OBcodigo_Click()
lblingrese.Caption = "Ingrese Codigo del articulo"
End Sub

Private Sub OBdescripcion_Click()
lblingrese.Caption = "Ingrese la descripcion del articulo"
End Sub
Private Sub txtbuscar_Change()
Tama�o_Grilla_articulos
If OBcodigo.Value = True Then
Buscar_articulo = "Codigo"
End If
If OBdescripcion.Value = True Then
Buscar_articulo = "Descripcion"
End If
funcion_Busqueda_cuadro
End Sub
Sub Tama�o_Grilla_articulos()
FGArticulos.Rows = 1
With FGArticulos
.Cols = 7
.ColWidth(0) = 0
.ColWidth(1) = 930
.ColWidth(2) = 3700
.ColWidth(3) = 1500
.ColWidth(4) = 1000
.ColWidth(5) = 1000
.ColWidth(6) = 1035
.TextArray(0) = "ID"
.TextArray(1) = "Codigo"
.TextArray(2) = "Descripci�n"
.TextArray(3) = "Rubro"
.TextArray(4) = "Stock"
.TextArray(5) = "Precio costo"
.TextArray(6) = "Precio venta"
.ColAlignment(5) = flexAlignRightCenter
.ColAlignment(6) = flexAlignRightCenter
End With
End Sub
Sub ActualizarGrilla()
cn.Open
Set rs = cn.Execute("SELECT Articulos.idarticulos, Articulos.codigo, Articulos.descripcion, Articulos.precio_venta, Articulos.precio_costo, Articulos.stock, Rubro.rubro FROM Articulos INNER JOIN Rubro ON Articulos.idrubro = Rubro.idrubro  ORDER By descripcion ASC")
With FGArticulos
.Rows = 1
Do While rs.EOF = False
.Rows = .Rows + 1
.TextMatrix(.Rows - 1, 0) = rs!idarticulos
.TextMatrix(.Rows - 1, 1) = rs!codigo
.TextMatrix(.Rows - 1, 2) = rs!descripcion
.TextMatrix(.Rows - 1, 3) = rs!Rubro
.TextMatrix(.Rows - 1, 4) = rs!stock
.TextMatrix(.Rows - 1, 5) = rs!precio_costo
.TextMatrix(.Rows - 1, 6) = rs!precio_venta
rs.MoveNext
Loop
End With
rs.Close
Set rs = Nothing
cn.Close
End Sub
Sub funcion_Busqueda_cuadro()
For i = 0 To 6
FGArticulos.Col = i
FGArticulos.CellBackColor = &H8000000F
Next i
cn.Open
If cmbfiltro.ListIndex = -1 Then
Set rs = cn.Execute("Select  Articulos.idarticulos, Articulos.codigo, Articulos.descripcion, Articulos.precio_venta, Articulos.precio_costo, Articulos.stock, Articulos.idrubro, Rubro.rubro FROM Articulos INNER JOIN Rubro ON Articulos.idrubro = Rubro.idrubro where " & Buscar_articulo & " like" & " '" & "" & txtbuscar.Text & "%' ORDER By descripcion ASC")
Else
Set rs = cn.Execute("Select  Articulos.idarticulos, Articulos.codigo, Articulos.descripcion, Articulos.precio_venta, Articulos.precio_costo, Articulos.stock, Articulos.idrubro, Rubro.rubro FROM Articulos INNER JOIN Rubro ON Articulos.idrubro = Rubro.idrubro where " & Buscar_articulo & " like" & " '" & "" & txtbuscar.Text & "%' AND articulos.idrubro = " & cmbfiltro.ListIndex + 1 & " ORDER By descripcion ASC")
End If
Do While rs.EOF = False
With FGArticulos
.Rows = .Rows + 1
.TextMatrix(.Rows - 1, 0) = rs!idarticulos
.TextMatrix(.Rows - 1, 1) = rs!codigo
.TextMatrix(.Rows - 1, 2) = rs!descripcion
.TextMatrix(.Rows - 1, 3) = rs!Rubro
.TextMatrix(.Rows - 1, 4) = rs!stock
.TextMatrix(.Rows - 1, 5) = rs!precio_costo
.TextMatrix(.Rows - 1, 6) = rs!precio_venta
End With
rs.MoveNext
Loop
rs.Close
Set rs = Nothing
cn.Close
End Sub
Sub filtro_articulos()
cn.Open
Set rs = cn.Execute("Listado_Rubro")
Do While rs.EOF = False
cmbfiltro.AddItem rs!Rubro
cmbfiltro.ItemData(cmbfiltro.NewIndex) = rs!idrubro
rs.MoveNext
Loop
rs.Close
Set rs = Nothing
cn.Close
End Sub
Private Sub cmdcancelar_articulos_Click()
frm_facturacion.Width = 10275
cmdbuscararticulos.Enabled = True
End Sub
Sub calcular_totales()
neto = 0
For i = 1 To FGDetalle.Rows - 1
neto = neto + FGDetalle.TextMatrix(i, 5)
Next i
iva = neto * 21 / 100
total = neto + iva
If lbltipofactura.Caption = "B" Then
txtneto.Text = total
txtiva.Text = ""
txttotal.Text = total
Else
txtneto.Text = neto
txtiva.Text = iva
txttotal.Text = total
End If
End Sub
Sub Estado_Inicial()
'cliente
lblcodigo.Caption = ""
lblnombre.Caption = ""
lbldomicilio.Caption = ""
lblcuit.Caption = ""
lblcondicion.Caption = ""
lblsaldo.Caption = ""
Borderusuario.BorderColor = &HFF&
'encabezado
lblnfactura.Caption = "0000000000000"
lbltipofactura.Caption = "A"
cmbcomprobante.Text = "[Seleccione]"
OBContado.Value = True
OBContado.Enabled = True
OBctacorriente.Value = False
color_encabezado
'detalle
FGDetalle.Rows = 1
borderdetalle.BorderColor = &H0&
'totales
txtneto.Text = ""
txtiva.Text = ""
txttotal.Text = ""
'marcos
frmcliente.Enabled = True
frmencabezado.Enabled = False
frmdetalle.Enabled = False
'articulos
Tama�o_Grilla_articulos
ActualizarGrilla
OBcodigo.Value = True
cmbfiltro.Text = "[Seleccione]"
cmdagregar.Enabled = True
cmdmodificar.Enabled = True
cmdeliminar.Enabled = True
'formulario
frm_facturacion.Width = 10275
End Sub
Function combos_letras(ByVal KeyAscii As Integer) As Integer
combos_letras = 0
End Function
Sub color_encabezado()
If Borderusuario.BorderColor = &HFF& Or borderdetalle.BorderColor = &HFF& Then
borderencabezado1.BorderColor = &H0&
borderencabezado2.BorderColor = &H0&
borderencabezado3.BorderColor = &H0&
borderencabezado4.BorderColor = &H0&
borderencabezado5.BorderColor = &H0&
borderencabezado6.BorderColor = &H0&
Else
borderencabezado1.BorderColor = &HFF&
borderencabezado2.BorderColor = &HFF&
borderencabezado3.BorderColor = &HFF&
borderencabezado4.BorderColor = &HFF&
borderencabezado5.BorderColor = &HFF&
borderencabezado6.BorderColor = &HFF&
End If
End Sub
