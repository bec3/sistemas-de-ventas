VERSION 5.00
Begin VB.Form frm_busqueda_articulos_cantidad 
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cantidad"
   ClientHeight    =   3540
   ClientLeft      =   9900
   ClientTop       =   4695
   ClientWidth     =   4485
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "frm_busqueda_articulos_cantidad.frx":0000
   ScaleHeight     =   3540
   ScaleWidth      =   4485
   Begin VB.Frame Frame2 
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      Caption         =   "Frame2"
      Height          =   2535
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Width           =   4215
      Begin VB.TextBox txtdescripcion 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         Height          =   375
         Left            =   120
         TabIndex        =   7
         Top             =   360
         Width           =   3975
      End
      Begin VB.TextBox txtcantidad 
         Appearance      =   0  'Flat
         Height          =   375
         Left            =   1320
         TabIndex        =   6
         Top             =   1200
         Width           =   1575
      End
      Begin VB.TextBox txtunitario 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         Height          =   375
         Left            =   120
         TabIndex        =   5
         Top             =   2040
         Width           =   1935
      End
      Begin VB.TextBox txttotal 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         Height          =   375
         Left            =   2160
         TabIndex        =   4
         Top             =   2040
         Width           =   1935
      End
      Begin VB.Shape Shape2 
         Height          =   2535
         Left            =   0
         Top             =   0
         Width           =   4215
      End
      Begin VB.Label Cantidad 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Cantidad"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1680
         TabIndex        =   11
         Top             =   840
         Width           =   765
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Precio Unitario"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   480
         TabIndex        =   10
         Top             =   1680
         Width           =   1275
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Precio Total"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   2520
         TabIndex        =   9
         Top             =   1680
         Width           =   1035
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Descripción"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1560
         TabIndex        =   8
         Top             =   0
         Width           =   1020
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   615
      Left            =   120
      TabIndex        =   0
      Top             =   2760
      Width           =   4215
      Begin VB.CommandButton cmdcancelar 
         Caption         =   "Cancelar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2160
         TabIndex        =   2
         Top             =   120
         Width           =   1935
      End
      Begin VB.CommandButton cmdaceptar 
         Caption         =   "Aceptar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   1
         Top             =   120
         Width           =   1935
      End
      Begin VB.Shape Shape1 
         Height          =   615
         Left            =   0
         Top             =   0
         Width           =   4215
      End
   End
End
Attribute VB_Name = "frm_busqueda_articulos_cantidad"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public posicion_modificar As Integer
Dim total As Double
Private Sub cmdaceptar_Click()
If txtcantidad.Text = "0" Or txtcantidad.Text = "" Then
    MsgBox ("Error, ingrese una cantidad")
Else
total = txttotal.Text
    If frm_facturacion.EstadoDetalle = "modificar" Then
    Modificar 'Funcion modificar
    Else
    Agregar 'Funcion agregar
    End If
End If
frm_facturacion.calcular_totales
End Sub
Sub Agregar()
With frm_facturacion
repetido = 0
For i = 1 To .FGDetalle.Rows - 1
If .FGDetalle.TextMatrix(i, 1) = .FGArticulos.TextMatrix(posicion_modificar, 1) Then
repetido = i
End If
Next i

If .cmbcomprobante.Text = "Nota de Credito" Then
    If repetido = 0 Then
        .FGDetalle.Rows = .FGDetalle.Rows + 1
        .FGDetalle.TextMatrix(.FGDetalle.Rows - 1, 0) = .FGArticulos.TextMatrix(posicion_modificar, 0)
        .FGDetalle.TextMatrix(.FGDetalle.Rows - 1, 1) = .FGArticulos.TextMatrix(posicion_modificar, 1)
        .FGDetalle.TextMatrix(.FGDetalle.Rows - 1, 2) = .FGArticulos.TextMatrix(posicion_modificar, 2)
        .FGDetalle.TextMatrix(.FGDetalle.Rows - 1, 3) = .FGArticulos.TextMatrix(posicion_modificar, 6)
        .FGDetalle.TextMatrix(.FGDetalle.Rows - 1, 4) = txtcantidad.Text
        .FGDetalle.TextMatrix(.FGDetalle.Rows - 1, 5) = total
    Else
        .FGDetalle.TextMatrix(.FGDetalle.Rows - 1, 4) = Val(.FGDetalle.TextMatrix(repetido, 4)) + Val(txtcantidad.Text)
        .FGDetalle.TextMatrix(.FGDetalle.Rows - 1, 5) = .FGDetalle.TextMatrix(repetido, 5) + total
    End If
    .FGArticulos.SetFocus
    txtcantidad.Text = ""
    frm_busqueda_articulos_cantidad.Hide
Else
    If .FGArticulos.TextMatrix(posicion_modificar, 4) >= Val(txtcantidad.Text) Then
        .FGArticulos.TextMatrix(posicion_modificar, 4) = .FGArticulos.TextMatrix(posicion_modificar, 4) - txtcantidad.Text
        If repetido = 0 Then
            .FGDetalle.Rows = .FGDetalle.Rows + 1
            .FGDetalle.TextMatrix(.FGDetalle.Rows - 1, 0) = .FGArticulos.TextMatrix(posicion_modificar, 0)
            .FGDetalle.TextMatrix(.FGDetalle.Rows - 1, 1) = .FGArticulos.TextMatrix(posicion_modificar, 1)
            .FGDetalle.TextMatrix(.FGDetalle.Rows - 1, 2) = .FGArticulos.TextMatrix(posicion_modificar, 2)
            .FGDetalle.TextMatrix(.FGDetalle.Rows - 1, 3) = .FGArticulos.TextMatrix(posicion_modificar, 6)
            .FGDetalle.TextMatrix(.FGDetalle.Rows - 1, 4) = txtcantidad.Text
            .FGDetalle.TextMatrix(.FGDetalle.Rows - 1, 5) = total
        Else
        .FGDetalle.TextMatrix(.FGDetalle.Rows - 1, 4) = Val(.FGDetalle.TextMatrix(repetido, 4)) + Val(txtcantidad.Text)
        .FGDetalle.TextMatrix(.FGDetalle.Rows - 1, 5) = Val(.FGDetalle.TextMatrix(repetido, 5)) + Val(txttotal.Text)
        End If
        .FGArticulos.SetFocus
        txtcantidad.Text = ""
        frm_busqueda_articulos_cantidad.Hide
    Else
         MsgBox ("Limite del stock superado")
    End If
End If
End With
End Sub
Sub Modificar()
With frm_facturacion
For i = 1 To .FGArticulos.Rows - 1
    If .FGArticulos.TextMatrix(i, 2) = .FGDetalle.TextMatrix(.FGDetalle.Row, 2) Then
        marca = i
    End If
Next i
If .cmbcomprobante.Text = "Nota de Credito" Then
    .FGDetalle.TextMatrix(posicion_modificar, 4) = txtcantidad.Text
    .FGDetalle.TextMatrix(posicion_modificar, 5) = total
    txtcantidad.Text = ""
    frm_busqueda_articulos_cantidad.Hide
Else
    If .FGArticulos.TextMatrix(marca, 4) + Val(.FGDetalle.TextMatrix(posicion_modificar, 4)) >= Val(txtcantidad.Text) Then
        .FGArticulos.TextMatrix(marca, 4) = Val(.FGArticulos.TextMatrix(marca, 4)) + Val(.FGDetalle.TextMatrix(posicion_modificar, 4))
        .FGArticulos.TextMatrix(marca, 4) = Val(.FGArticulos.TextMatrix(marca, 4)) - Val(txtcantidad.Text)
        .FGDetalle.TextMatrix(posicion_modificar, 4) = txtcantidad.Text
        .FGDetalle.TextMatrix(posicion_modificar, 5) = total
        txtcantidad.Text = ""
        frm_busqueda_articulos_cantidad.Hide
    Else
        MsgBox ("Limite del stock superado")
    End If
End If
End With
End Sub
Private Sub cmdcancelar_Click()
Unload Me
End Sub

Private Sub txtcantidad_KeyPress(KeyAscii As Integer)
strvalid = "0123456789"
KeyAscii = Asc(Chr(KeyAscii))
If (KeyAscii > 10) Then
If InStr(strvalid, Chr(KeyAscii)) = 0 Then
KeyAscii = 0
End If
End If
End Sub
Private Sub txtcantidad_Change()
If txtcantidad.Text <> "" Then
Dim Cantidad As Integer
Dim unitario As Double
Cantidad = txtcantidad.Text
unitario = txtunitario.Text
txttotal.Text = Cantidad * unitario
Else
txttotal = ""
End If
End Sub
