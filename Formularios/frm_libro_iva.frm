VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "mshflxgd.ocx"
Begin VB.Form frm_libro_iva 
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Libro Iva"
   ClientHeight    =   8910
   ClientLeft      =   945
   ClientTop       =   1095
   ClientWidth     =   15345
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "frm_libro_iva.frx":0000
   ScaleHeight     =   8910
   ScaleWidth      =   15345
   Begin VB.OptionButton obsemana 
      BackColor       =   &H00C0C0C0&
      Caption         =   "�ltimos 7 d�as"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   1200
      TabIndex        =   15
      Top             =   2280
      Width           =   1695
   End
   Begin VB.OptionButton obmes 
      BackColor       =   &H00C0C0C0&
      Caption         =   "Este mes"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   3120
      TabIndex        =   14
      Top             =   2280
      Value           =   -1  'True
      Width           =   1455
   End
   Begin VB.OptionButton obhoy 
      BackColor       =   &H00C0C0C0&
      Caption         =   "Hoy"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   240
      TabIndex        =   13
      Top             =   2280
      Width           =   1695
   End
   Begin VB.OptionButton obtodos 
      BackColor       =   &H00C0C0C0&
      Caption         =   "Todos"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   4680
      TabIndex        =   12
      Top             =   2280
      Width           =   1575
   End
   Begin VB.Frame Frame4 
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      Caption         =   "Frame4"
      Height          =   5175
      Left            =   120
      TabIndex        =   10
      Top             =   2880
      Width           =   15135
      Begin MSHierarchicalFlexGridLib.MSHFlexGrid FGLibroiva 
         Height          =   4935
         Left            =   120
         TabIndex        =   11
         Top             =   120
         Width           =   14895
         _ExtentX        =   26273
         _ExtentY        =   8705
         _Version        =   393216
         FixedCols       =   0
         ScrollBars      =   2
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _NumberOfBands  =   1
         _Band(0).Cols   =   2
      End
      Begin VB.Line Line12 
         X1              =   15120
         X2              =   15120
         Y1              =   0
         Y2              =   5400
      End
      Begin VB.Line Line11 
         X1              =   0
         X2              =   0
         Y1              =   0
         Y2              =   5160
      End
      Begin VB.Line Line7 
         X1              =   0
         X2              =   15360
         Y1              =   5160
         Y2              =   5160
      End
      Begin VB.Line Line6 
         X1              =   0
         X2              =   15360
         Y1              =   0
         Y2              =   0
      End
   End
   Begin VB.Frame Frame3 
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      Height          =   1935
      Left            =   120
      TabIndex        =   9
      Top             =   120
      Width           =   15135
      Begin VB.Line Line16 
         X1              =   15120
         X2              =   15120
         Y1              =   0
         Y2              =   1920
      End
      Begin VB.Line Line15 
         X1              =   0
         X2              =   0
         Y1              =   0
         Y2              =   1920
      End
      Begin VB.Line Line2 
         X1              =   0
         X2              =   15360
         Y1              =   1920
         Y2              =   1920
      End
      Begin VB.Line Line1 
         X1              =   0
         X2              =   15120
         Y1              =   0
         Y2              =   0
      End
      Begin VB.Image Image1 
         Height          =   1710
         Left            =   13320
         Picture         =   "frm_libro_iva.frx":6FD2
         Top             =   120
         Width           =   1695
      End
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      Height          =   615
      Left            =   120
      TabIndex        =   5
      Top             =   8160
      Width           =   15135
      Begin VB.TextBox txttotal 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   12825
         Locked          =   -1  'True
         TabIndex        =   8
         Top             =   120
         Width           =   1950
      End
      Begin VB.TextBox txtiva 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   10845
         Locked          =   -1  'True
         TabIndex        =   7
         Top             =   120
         Width           =   2000
      End
      Begin VB.TextBox txtneto 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   8865
         Locked          =   -1  'True
         TabIndex        =   6
         Top             =   120
         Width           =   1995
      End
      Begin VB.Line Line14 
         X1              =   15120
         X2              =   15120
         Y1              =   0
         Y2              =   600
      End
      Begin VB.Line Line13 
         X1              =   0
         X2              =   0
         Y1              =   0
         Y2              =   600
      End
      Begin VB.Line Line8 
         X1              =   0
         X2              =   15360
         Y1              =   600
         Y2              =   600
      End
      Begin VB.Line Line4 
         X1              =   0
         X2              =   15360
         Y1              =   0
         Y2              =   0
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      Caption         =   "Filtros"
      Height          =   615
      Left            =   120
      TabIndex        =   0
      Top             =   2160
      Width           =   15135
      Begin MSComCtl2.DTPicker DTDesde 
         Height          =   375
         Left            =   10200
         TabIndex        =   1
         Top             =   120
         Width           =   1815
         _ExtentX        =   3201
         _ExtentY        =   661
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   89587713
         CurrentDate     =   36161
      End
      Begin MSComCtl2.DTPicker DTHasta 
         Height          =   375
         Left            =   12960
         TabIndex        =   2
         Top             =   120
         Width           =   1815
         _ExtentX        =   3201
         _ExtentY        =   661
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   89587713
         CurrentDate     =   41529
      End
      Begin VB.Line Line10 
         X1              =   15120
         X2              =   15120
         Y1              =   0
         Y2              =   600
      End
      Begin VB.Line Line9 
         X1              =   0
         X2              =   0
         Y1              =   0
         Y2              =   600
      End
      Begin VB.Line Line5 
         X1              =   0
         X2              =   15360
         Y1              =   600
         Y2              =   600
      End
      Begin VB.Line Line3 
         X1              =   0
         X2              =   15360
         Y1              =   0
         Y2              =   0
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Desde"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   9360
         TabIndex        =   4
         Top             =   120
         Width           =   705
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Hasta"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   12240
         TabIndex        =   3
         Top             =   120
         Width           =   615
      End
   End
End
Attribute VB_Name = "frm_libro_iva"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim neto As Double
Dim total As Double
Dim iva As Double
Private Sub dtdesde_change()
Actualizar_Grilla
End Sub
Private Sub dthasta_change()
Actualizar_Grilla
End Sub

Private Sub FGLibroiva_dblClick()
If FGLibroiva.Row <> 0 Then
frm_detalle_ventas.Detalle_venta_libro_iva
frm_detalle_ventas.Form_Load
frm_detalle_ventas.Show
End If
End Sub

Private Sub Form_Load()
DTHasta.Value = Date
Tama�o_Grilla
Actualizar_Grilla
obmes_Click
End Sub
Sub Tama�o_Grilla()
With FGLibroiva
.Cols = 8
.ColWidth(0) = 1000
.ColWidth(1) = 500
.ColWidth(2) = 1500
.ColWidth(3) = 1500
.ColWidth(4) = 2730
.ColWidth(5) = 1500
.ColWidth(6) = 2000
.ColWidth(7) = 2000
.ColWidth(8) = 1850
End With
End Sub
Sub Actualizar_Grilla()
cn.Open
Set rs = cn.Execute("Listado_libro_iva '" & DTDesde.Value & "','" & DTHasta.Value & "'")
Set FGLibroiva.DataSource = rs
rs.Close
Set rs = Nothing
cn.Close
neto = 0
iva = 0
total = 0
For i = 1 To FGLibroiva.Rows - 1
If FGLibroiva.TextMatrix(i, 2) = "Nota de Credito" Then
neto = neto - Val(FGLibroiva.TextMatrix(i, 6))
iva = iva - Val(FGLibroiva.TextMatrix(i, 7))
total = total - Val(FGLibroiva.TextMatrix(i, 8))
Else
neto = neto + Val(FGLibroiva.TextMatrix(i, 6))
iva = iva + Val(FGLibroiva.TextMatrix(i, 7))
total = total + Val(FGLibroiva.TextMatrix(i, 8))
End If
Next i
txtneto.Text = neto
txtiva.Text = iva
txttotal.Text = total
FGLibroiva.ColAlignment(6) = flexAlignRightCenter
FGLibroiva.ColAlignment(7) = flexAlignRightCenter
FGLibroiva.ColAlignment(8) = flexAlignRightCenter
End Sub
Private Sub obhoy_Click()
DTDesde.Value = Date
Actualizar_Grilla
End Sub

Private Sub obmes_Click()
DTDesde.Value = "1/" & Month(Date) & "/" & Year(Date)
Actualizar_Grilla
End Sub
Private Sub obsemana_Click()
DTDesde.Value = Date - 7
 Actualizar_Grilla
End Sub
Private Sub obtodos_Click()
DTDesde.Value = "1/1/1999"
Actualizar_Grilla
DTDesde.Value = FGLibroiva.TextMatrix(1, 0)
End Sub

