VERSION 5.00
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "mshflxgd.ocx"
Begin VB.Form frm_detalle_ventas 
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Detalle de venta"
   ClientHeight    =   4440
   ClientLeft      =   1830
   ClientTop       =   3795
   ClientWidth     =   10455
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "frm_detalle_venta.frx":0000
   ScaleHeight     =   4440
   ScaleWidth      =   10455
   Begin MSHierarchicalFlexGridLib.MSHFlexGrid FGDetalle 
      Height          =   3255
      Left            =   240
      TabIndex        =   0
      Top             =   240
      Width           =   10005
      _ExtentX        =   17648
      _ExtentY        =   5741
      _Version        =   393216
      FixedCols       =   0
      ScrollBars      =   2
      Appearance      =   0
      BeginProperty FontFixed {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _NumberOfBands  =   1
      _Band(0).Cols   =   2
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      Height          =   3495
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   10215
      Begin VB.Line Line5 
         X1              =   10200
         X2              =   10200
         Y1              =   3480
         Y2              =   0
      End
      Begin VB.Line Line4 
         X1              =   10440
         X2              =   0
         Y1              =   0
         Y2              =   0
      End
      Begin VB.Line Line3 
         X1              =   0
         X2              =   0
         Y1              =   0
         Y2              =   3480
      End
      Begin VB.Line Line2 
         X1              =   0
         X2              =   10200
         Y1              =   3480
         Y2              =   3480
      End
   End
   Begin VB.Frame frmtotales 
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      Height          =   615
      Left            =   120
      TabIndex        =   2
      Top             =   3720
      Width           =   10215
      Begin VB.CommandButton cmdsalir 
         Caption         =   "Salir"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   9000
         TabIndex        =   6
         Top             =   120
         Width           =   1095
      End
      Begin VB.TextBox txtneto 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   600
         TabIndex        =   5
         Top             =   90
         Width           =   2535
      End
      Begin VB.TextBox txtiva 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   3600
         TabIndex        =   4
         Top             =   90
         Width           =   2055
      End
      Begin VB.TextBox txttotal 
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   6240
         TabIndex        =   3
         Top             =   90
         Width           =   2535
      End
      Begin VB.Line Line9 
         X1              =   10200
         X2              =   10200
         Y1              =   0
         Y2              =   720
      End
      Begin VB.Line Line8 
         X1              =   0
         X2              =   0
         Y1              =   0
         Y2              =   600
      End
      Begin VB.Line Line7 
         X1              =   0
         X2              =   10200
         Y1              =   600
         Y2              =   600
      End
      Begin VB.Line Line6 
         X1              =   0
         X2              =   10200
         Y1              =   0
         Y2              =   0
      End
      Begin VB.Line Line1 
         BorderColor     =   &H80000007&
         X1              =   8880
         X2              =   8880
         Y1              =   0
         Y2              =   600
      End
      Begin VB.Label Label8 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Total"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   5760
         TabIndex        =   9
         Top             =   120
         Width           =   420
      End
      Begin VB.Label Label9 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Iva"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   3240
         TabIndex        =   8
         Top             =   120
         Width           =   225
      End
      Begin VB.Label Label10 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Neto"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   120
         TabIndex        =   7
         Top             =   120
         Width           =   405
      End
   End
End
Attribute VB_Name = "frm_detalle_ventas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim idventas As Integer
Dim neto As Double
Dim iva As Double
Dim total As Double
Private Sub cmdsalir_Click()
Unload Me
End Sub
Public Sub Form_Load()
Tama�o_Grilla
Actualizar_Grilla
neto = 0
For i = 1 To FGDetalle.Rows - 1
neto = neto + (Val(FGDetalle.TextMatrix(i, 1)) * Val(FGDetalle.TextMatrix(i, 3)))
Next i
iva = neto * 21 / 100
total = neto + iva
txtneto.Text = neto
txtiva.Text = iva
txttotal.Text = total
End Sub
Sub Actualizar_Grilla()
For i = 0 To 4
FGDetalle.Col = i
FGDetalle.CellBackColor = &H8000000F
Next i
FGDetalle.Rows = 1
cn.Open
Set rs = cn.Execute("Listado_Detalle_venta " & idventas & "")
Do While rs.EOF = False
With FGDetalle
.Rows = .Rows + 1
.TextMatrix(.Rows - 1, 0) = rs!descripcion
.TextMatrix(.Rows - 1, 1) = rs!precio_venta
.TextMatrix(.Rows - 1, 2) = rs!precio_costo
.TextMatrix(.Rows - 1, 3) = rs!Cantidad
.TextMatrix(.Rows - 1, 4) = rs!total
End With
rs.MoveNext
Loop
rs.Close
Set rs = Nothing
cn.Close
End Sub
Sub Tama�o_Grilla()
With FGDetalle
.Cols = 5
.ColWidth(0) = 4000
.TextArray(0) = "Descripci�n"
.ColWidth(1) = 1500
.TextArray(1) = "Precio de venta"
.ColWidth(2) = 1500
.TextArray(2) = "Precio de compra"
.ColWidth(3) = 1500
.TextArray(3) = "Cantidad"
.ColWidth(4) = 1500
.TextArray(4) = "Total"
.ColAlignment(1) = flexAlignRightCenter
.ColAlignment(2) = flexAlignRightCenter
.ColAlignment(4) = flexAlignRightCenter
End With
End Sub
Public Sub Detalle_venta_cuenta_corriente()
Dim detalle() As String
Dim letra As String
Dim numero As String
idventas = 0
detalle = Split(frm_cuenta_corriente.FGCuentacorriente.TextMatrix(frm_cuenta_corriente.FGCuentacorriente.Row, 1), " ")
If detalle(0) <> "Recibo" Then
If detalle(0) = "Nota" Then
letra = detalle(3)
numero = detalle(4)
Else
letra = detalle(1)
numero = detalle(2)
End If
cn.Open
Set rs = cn.Execute("select idventas from ventas where letra_c = '" & letra & "' AND numero = '" & numero & "'")
idventas = rs!idventas
rs.Close
Set rs = Nothing
cn.Close
End If
End Sub
Public Sub Detalle_venta_libro_iva()
cn.Open
Set rs = cn.Execute("select idventas from ventas where letra_c = '" & frm_libro_iva.FGLibroiva.TextMatrix(frm_libro_iva.FGLibroiva.Row, 1) & "' AND numero = '" & frm_libro_iva.FGLibroiva.TextMatrix(frm_libro_iva.FGLibroiva.Row, 3) & "'")
idventas = rs!idventas
rs.Close
Set rs = Nothing
cn.Close
End Sub
Public Sub Detalle_venta_resumen()
cn.Open
Set rs = cn.Execute("select idventas from ventas where letra_c = '" & frmmenuprincipal.FGResumen.TextMatrix(frmmenuprincipal.FGResumen.Row, 1) & "' AND numero = '" & frmmenuprincipal.FGResumen.TextMatrix(frmmenuprincipal.FGResumen.Row, 3) & "'")
idventas = rs!idventas
rs.Close
Set rs = Nothing
cn.Close
End Sub
