VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "mshflxgd.ocx"
Begin VB.Form frm_cuenta_corriente 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cuenta Corriente"
   ClientHeight    =   10080
   ClientLeft      =   945
   ClientTop       =   1095
   ClientWidth     =   11055
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "frm_cuenta_corriente.frx":0000
   ScaleHeight     =   10080
   ScaleWidth      =   11055
   Begin VB.Frame Frame3 
      BackColor       =   &H8000000A&
      BorderStyle     =   0  'None
      Caption         =   "Frame3"
      Height          =   5895
      Left            =   120
      TabIndex        =   27
      Top             =   3360
      Width           =   10815
      Begin MSHierarchicalFlexGridLib.MSHFlexGrid FGCuentacorriente 
         Height          =   5655
         Left            =   120
         TabIndex        =   28
         Top             =   120
         Width           =   10575
         _ExtentX        =   18653
         _ExtentY        =   9975
         _Version        =   393216
         BackColor       =   16777215
         FixedCols       =   0
         Enabled         =   0   'False
         ScrollBars      =   2
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         _NumberOfBands  =   1
         _Band(0).Cols   =   2
      End
      Begin VB.Line Line19 
         X1              =   0
         X2              =   10920
         Y1              =   5880
         Y2              =   5880
      End
      Begin VB.Line Line18 
         X1              =   10800
         X2              =   10800
         Y1              =   0
         Y2              =   5880
      End
      Begin VB.Line Line17 
         X1              =   0
         X2              =   0
         Y1              =   6135
         Y2              =   0
      End
      Begin VB.Line Line16 
         X1              =   0
         X2              =   10800
         Y1              =   0
         Y2              =   0
      End
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      Height          =   615
      Left            =   120
      TabIndex        =   20
      Top             =   9360
      Width           =   10815
      Begin VB.Line Line6 
         X1              =   10800
         X2              =   10815
         Y1              =   0
         Y2              =   1575
      End
      Begin VB.Line Line5 
         X1              =   0
         X2              =   0
         Y1              =   0
         Y2              =   600
      End
      Begin VB.Line Line4 
         X1              =   0
         X2              =   10800
         Y1              =   600
         Y2              =   600
      End
      Begin VB.Line Line3 
         X1              =   0
         X2              =   10800
         Y1              =   0
         Y2              =   0
      End
      Begin VB.Label saldodesdefecha 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Saldo"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   120
         TabIndex        =   22
         Top             =   120
         Width           =   735
      End
      Begin VB.Line Line1 
         BorderColor     =   &H80000007&
         X1              =   5520
         X2              =   5520
         Y1              =   0
         Y2              =   720
      End
      Begin VB.Label saldohastafecha 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Saldo"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   5640
         TabIndex        =   21
         Top             =   120
         Width           =   735
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      Caption         =   "Filtros"
      Height          =   975
      Left            =   120
      TabIndex        =   14
      Top             =   2280
      Width           =   10815
      Begin VB.OptionButton obtodos 
         BackColor       =   &H00C0C0C0&
         Caption         =   "Todos"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   1920
         TabIndex        =   29
         Top             =   480
         Width           =   1575
      End
      Begin VB.OptionButton obhoy 
         BackColor       =   &H00C0C0C0&
         Caption         =   "Hoy"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   26
         Top             =   120
         Width           =   1695
      End
      Begin VB.OptionButton obmes 
         BackColor       =   &H00C0C0C0&
         Caption         =   "Este mes"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1920
         TabIndex        =   25
         Top             =   120
         Width           =   1455
      End
      Begin VB.OptionButton obsemana 
         BackColor       =   &H00C0C0C0&
         Caption         =   "�ltimos 7 d�as"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   120
         TabIndex        =   24
         Top             =   360
         Width           =   1695
      End
      Begin MSComCtl2.DTPicker DTDesde 
         Height          =   375
         Left            =   6120
         TabIndex        =   17
         Top             =   240
         Width           =   1815
         _ExtentX        =   3201
         _ExtentY        =   661
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   48168961
         CurrentDate     =   36161
      End
      Begin MSComCtl2.DTPicker DTHasta 
         Height          =   375
         Left            =   8880
         TabIndex        =   16
         Top             =   240
         Width           =   1815
         _ExtentX        =   3201
         _ExtentY        =   661
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   48168961
         CurrentDate     =   41529
      End
      Begin VB.Line Line15 
         X1              =   10800
         X2              =   10800
         Y1              =   0
         Y2              =   1200
      End
      Begin VB.Line Line14 
         X1              =   0
         X2              =   0
         Y1              =   0
         Y2              =   960
      End
      Begin VB.Line Line13 
         X1              =   0
         X2              =   10800
         Y1              =   0
         Y2              =   0
      End
      Begin VB.Line Line12 
         X1              =   0
         X2              =   11400
         Y1              =   960
         Y2              =   960
      End
      Begin VB.Line Line2 
         BorderColor     =   &H80000007&
         X1              =   5160
         X2              =   5160
         Y1              =   0
         Y2              =   960
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Hasta"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   8160
         TabIndex        =   19
         Top             =   240
         Width           =   615
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Desde"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   5280
         TabIndex        =   18
         Top             =   240
         Width           =   705
      End
   End
   Begin VB.Frame frmcliente 
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'None
      Caption         =   "Cliente"
      Height          =   2055
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   10815
      Begin VB.CommandButton cmdpago 
         Caption         =   "Realizar Pago"
         BeginProperty Font 
            Name            =   "Calibri"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   9000
         TabIndex        =   23
         Top             =   1560
         Width           =   1695
      End
      Begin VB.CommandButton cmdbuscar 
         Caption         =   "Buscar"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   1
         Top             =   120
         Width           =   1095
      End
      Begin VB.Line Line11 
         X1              =   10800
         X2              =   10800
         Y1              =   0
         Y2              =   2040
      End
      Begin VB.Line Line10 
         X1              =   0
         X2              =   0
         Y1              =   0
         Y2              =   2040
      End
      Begin VB.Line Line9 
         X1              =   0
         X2              =   0
         Y1              =   0
         Y2              =   2040
      End
      Begin VB.Line Line8 
         X1              =   0
         X2              =   23055
         Y1              =   2040
         Y2              =   2055
      End
      Begin VB.Line Line7 
         X1              =   0
         X2              =   10800
         Y1              =   0
         Y2              =   0
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Condici�n IVA:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   1560
         TabIndex        =   13
         Top             =   1560
         Width           =   1560
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Domicilio:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   1560
         TabIndex        =   12
         Top             =   840
         Width           =   1050
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "CUIT:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   1560
         TabIndex        =   11
         Top             =   1200
         Width           =   585
      End
      Begin VB.Label Label7 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Apellido y nombre:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   1560
         TabIndex        =   10
         Top             =   480
         Width           =   1935
      End
      Begin VB.Label lblnombre 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "nombre"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   3600
         TabIndex        =   9
         Top             =   480
         Width           =   795
      End
      Begin VB.Label lblcuit 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Cuit"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   2160
         TabIndex        =   8
         Top             =   1200
         Width           =   420
      End
      Begin VB.Label lbldomicilio 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Domicilio"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   2640
         TabIndex        =   7
         Top             =   840
         Width           =   990
      End
      Begin VB.Label lblcondicion 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Condicion IVA"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   3240
         TabIndex        =   6
         Top             =   1560
         Width           =   1500
      End
      Begin VB.Label Label12 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Saldo Total:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   20.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   480
         Left            =   6120
         TabIndex        =   5
         Top             =   120
         Width           =   2175
      End
      Begin VB.Label lblsaldo 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Saldo"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   20.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   480
         Left            =   8280
         TabIndex        =   4
         Top             =   120
         Width           =   1035
      End
      Begin VB.Label lblcodigo 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "codigo"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   2400
         TabIndex        =   3
         Top             =   120
         Width           =   720
      End
      Begin VB.Label Label13 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Codigo:"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   1560
         TabIndex        =   2
         Top             =   120
         Width           =   840
      End
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   270
      Left            =   6840
      TabIndex        =   15
      Top             =   9840
      Width           =   60
   End
End
Attribute VB_Name = "frm_cuenta_corriente"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public saldo As Double
Private Sub cmdbuscar_Click()
Formulario = "cuentacorriente"
frm_busqueda_clientes.Show
End Sub
Private Sub cmdpago_Click()
If lblcodigo.Caption = "codigo" Then
MsgBox ("No se pueden realizar pagos sin la selecci�n previa de un cliente")
Else
frm_recibo.Show
frm_recibo.lblcodigo.Caption = lblcodigo.Caption
frm_recibo.lblnombre.Caption = lblnombre.Caption
frm_recibo.lbldomicilio.Caption = lbldomicilio.Caption
frm_recibo.lblcuit.Caption = lblcuit.Caption
frm_recibo.lblcondicion.Caption = lblcondicion.Caption
frm_recibo.Actualizar_Grilla
Unload Me
End If
End Sub

Private Sub dtdesde_change()
If lblsaldo.Caption <> "Saldo" Then
ActualizarLista
End If
End Sub
Private Sub dthasta_change()
If lblsaldo.Caption <> "Saldo" Then
ActualizarLista
End If
End Sub

Private Sub FGCuentacorriente_DblClick()
If FGCuentacorriente.Rows > 1 Then
frm_detalle_ventas.Detalle_venta_cuenta_corriente
frm_detalle_ventas.Form_Load
frm_detalle_ventas.Show
End If
End Sub
Private Sub Form_Load()
DTHasta.Value = Date
FGCuentacorriente.Enabled = False
End Sub
Public Sub ActualizarLista()
Tama�o_Grilla
cn.Open
Set rs = cn.Execute("Resumen_Cuenta_Corriente " & lblcodigo.Caption & ", '" & DTDesde.Value & "', '" & DTHasta.Value & "' ")
Set FGCuentacorriente.DataSource = rs
rs.Close
Set rs = Nothing
cn.Close
FGCuentacorriente.Cols = FGCuentacorriente.Cols + 1
FGCuentacorriente.TextArray(4) = "Saldo"
saldo_total = 0
For i = 1 To FGCuentacorriente.Rows - 1
saldo_total = saldo_total + Val(FGCuentacorriente.TextMatrix(i, 2)) - Val(FGCuentacorriente.TextMatrix(i, 3))
FGCuentacorriente.TextMatrix(i, 4) = saldo_total
Next i
If FGCuentacorriente.TextMatrix(FGCuentacorriente.Rows - 1, 4) = "Saldo" Then
saldohastafecha.Caption = "Saldo al " & DTHasta.Value & " = " & 0
saldodesdefecha.Caption = "Saldo al " & DTDesde.Value & " = " & saldo
Else
saldohastafecha.Caption = "Saldo al " & DTHasta.Value & " = " & FGCuentacorriente.TextMatrix(FGCuentacorriente.Rows - 1, 4)
saldoanterior = 0
cn.Open
Set rs = cn.Execute("Select Debe,Haber from cuenta_corriente where fecha <= '" & DTDesde.Value & "' AND idcliente = " & lblcodigo.Caption & " ")
Do While rs.EOF = False
saldoanterior = saldoanterior + (rs!debe - rs!haber)
rs.MoveNext
Loop
rs.Close
Set rs = Nothing
cn.Close
saldodesdefecha.Caption = "Saldo al " & DTDesde.Value & " = " & saldoanterior
End If
End Sub
Sub Tama�o_Grilla()
With FGCuentacorriente
.Cols = 6
.ColWidth(0) = 1000
.ColWidth(1) = 4300
.ColWidth(2) = 1500
.ColWidth(3) = 1500
.ColWidth(4) = 2000
End With
End Sub
Private Sub obhoy_Click()
DTDesde.Value = Date
dtdesde_change
End Sub

Private Sub obmes_Click()
DTDesde.Value = "1/" & Month(Date) & "/" & Year(Date)
dtdesde_change
End Sub

Private Sub obsemana_Click()
DTDesde.Value = Date - 7
dtdesde_change
End Sub
Private Sub obtodos_Click()
DTDesde.Value = "1/1/1999"
dtdesde_change
If FGCuentacorriente.Rows > 2 Then
DTDesde.Value = FGCuentacorriente.TextMatrix(1, 0)
End If
End Sub
